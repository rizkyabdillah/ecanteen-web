-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 18 Okt 2023 pada 19.10
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `VINZONE`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `BLOCKS`
--

CREATE TABLE `BLOCKS` (
  `ID_USER_ME` char(20) NOT NULL,
  `ID_USER_BLOCK` char(20) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `CHAT_ROOMS`
--

CREATE TABLE `CHAT_ROOMS` (
  `ID_CHAT_ROOM` char(25) NOT NULL,
  `ID_USER_SENDER` char(20) NOT NULL,
  `ID_USER_RECEIVER` char(20) NOT NULL,
  `LAST_CHAT_TIMESTAMP` datetime DEFAULT NULL,
  `IS_REQUESTED` tinyint(4) NOT NULL DEFAULT 1,
  `DELETED_AT_SENDER` datetime DEFAULT NULL,
  `DELETED_AT_RECEIVER` datetime DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `CHAT_ROOMS`
--

INSERT INTO `CHAT_ROOMS` (`ID_CHAT_ROOM`, `ID_USER_SENDER`, `ID_USER_RECEIVER`, `LAST_CHAT_TIMESTAMP`, `IS_REQUESTED`, `DELETED_AT_SENDER`, `DELETED_AT_RECEIVER`, `CREATED_AT`) VALUES
('CR-G4D5UBWY95RP0OIFG5MVXY', 'USRS-T5BIWEOWTB1WEJP', 'USRS-UVQ78B7A6A7MD2E', '2023-10-18 21:42:56', 1, '2023-10-19 00:00:47', NULL, '2023-10-13 14:16:36'),
('CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-1TXMVVUZU7E9B8A', 'USRS-T5BIWEOWTB1WEJP', '2023-10-18 21:34:29', 0, NULL, '2023-10-19 00:00:58', '2023-10-13 14:55:36'),
('CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'USRS-HDDY9VYYSAVSK1B', '2023-10-19 00:02:21', 0, '2023-10-19 00:01:12', NULL, '2023-10-12 15:28:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `DETAIL_CHATS`
--

CREATE TABLE `DETAIL_CHATS` (
  `ID_CHAT` char(30) NOT NULL,
  `ID_CHAT_ROOM` char(25) NOT NULL,
  `ID_USER_SENDER` char(20) NOT NULL,
  `CHAT` text DEFAULT NULL,
  `GAMBAR` varchar(85) DEFAULT NULL,
  `IS_SEEN` tinyint(1) NOT NULL DEFAULT 0,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `DETAIL_CHATS`
--

INSERT INTO `DETAIL_CHATS` (`ID_CHAT`, `ID_CHAT_ROOM`, `ID_USER_SENDER`, `CHAT`, `GAMBAR`, `IS_SEEN`, `CREATED_AT`) VALUES
('CHX-6WYBVZ3TIUSGJVMCXWUKVYKM7J', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'h', NULL, 1, '2023-10-12 15:28:14'),
('CHX-AM3ZFDTMZVAPN9ULUDOAFSBUQQ', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'Yoi', NULL, 1, '2023-10-18 16:58:57'),
('CHX-CU50OQXV9RQTU0SZSZYO6SSPH7', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-T5BIWEOWTB1WEJP', 'Oyi bro', NULL, 1, '2023-10-18 14:34:29'),
('CHX-DL7XHVMQBSDED1W8QUZDDI1LDE', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-HDDY9VYYSAVSK1B', 'Oyi bro', NULL, 1, '2023-10-18 17:02:21'),
('CHX-ED7YOIZOOYDL8G0J3NW0QEOPXM', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-T5BIWEOWTB1WEJP', 'Yoii', NULL, 1, '2023-10-13 14:56:00'),
('CHX-H11SKLUEFZGSG2CSWDOYXKYU82', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-T5BIWEOWTB1WEJP', 'xcd', '16972090651697209065_05b699dc700869e19d27.png', 1, '2023-10-13 14:57:45'),
('CHX-I1OSM3MNT25FJOSPOK7C8BJV9Z', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'Haloo', NULL, 1, '2023-10-18 17:01:21'),
('CHX-IGL3UL7BCCJWHBUXOZR8ZMDJ8A', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-HDDY9VYYSAVSK1B', 'Test....', NULL, 1, '2023-10-12 15:34:18'),
('CHX-J4GQTXSBTKN41CGW0O524DMWNL', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-1TXMVVUZU7E9B8A', 'Oyi bro', NULL, 1, '2023-10-18 14:22:28'),
('CHX-KC58YECNTJD9NX1IBHGXBCUDWS', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'Oyi bro', NULL, 1, '2023-10-18 16:27:19'),
('CHX-LSTCFUQECBICS1QL3Y0MATV5Q5', 'CR-VNWTTYOIIT6WBE2LNYXOGM', 'USRS-T5BIWEOWTB1WEJP', 'tedf', NULL, 1, '2023-10-12 15:28:36'),
('CHX-M47VQIFDJ68BRUOIW91PUTALEV', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-T5BIWEOWTB1WEJP', 'Bromo gaes', '16972095111697209511_c24d6d5a7244b4535242.png', 1, '2023-10-13 15:05:11'),
('CHX-PBU1OQHVUNI3ZXAYWYOBYUYIRE', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-1TXMVVUZU7E9B8A', 'Test....', NULL, 1, '2023-10-13 14:55:36'),
('CHX-QIQYYYJMOZYYYKYP17EEKSU1IW', 'CR-G4D5UBWY95RP0OIFG5MVXY', 'USRS-T5BIWEOWTB1WEJP', 'Oyi bro', NULL, 0, '2023-10-18 14:42:56'),
('CHX-RJ10J3WJVMTMPG3KALARZYFHKC', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-1TXMVVUZU7E9B8A', 'Oyi bro', NULL, 1, '2023-10-18 14:33:02'),
('CHX-URDFARO6LF89BDOGKBJQFMLLUH', 'CR-G4D5UBWY95RP0OIFG5MVXY', 'USRS-T5BIWEOWTB1WEJP', 'Haloges', NULL, 1, '2023-10-13 14:16:36'),
('CHX-VKTLWTNOJ8EKFIF4S1NVSH8IWQ', 'CR-NXFWCDU3WDQMHEJYP60VFB', 'USRS-T5BIWEOWTB1WEJP', 'Bromo gaes', '16972095051697209505_a5f6359292e48d1b2003.png', 1, '2023-10-13 15:05:05'),
('CHX-ZAMMGY0PMESQXVDF8AJJBDKWXJ', 'CR-G4D5UBWY95RP0OIFG5MVXY', 'USRS-T5BIWEOWTB1WEJP', 'Yolo', '16972096021697209602_8e547eada16f565b9dd8.png', 1, '2023-10-13 15:06:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `EVENTS`
--

CREATE TABLE `EVENTS` (
  `ID_EVENT` char(20) NOT NULL,
  `NAMA_EVENT` varchar(150) NOT NULL,
  `TANGGAL` date NOT NULL,
  `JAM_MULAI` time NOT NULL,
  `JAM_BERAKHIR` time NOT NULL,
  `JENIS_GAME` enum('MOBILE LEGEND','PUBG MOBILE','FREE FIRE','GENSHIN IMPACT','CLASH OF CLANS') NOT NULL,
  `TEMPAT_ACARA` text NOT NULL,
  `PENYEDIA` varchar(100) NOT NULL,
  `TELP_DIHUBUNGI` char(13) NOT NULL,
  `PERATURAN` text NOT NULL,
  `GAMBAR` varchar(85) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `EVENTS`
--

INSERT INTO `EVENTS` (`ID_EVENT`, `NAMA_EVENT`, `TANGGAL`, `JAM_MULAI`, `JAM_BERAKHIR`, `JENIS_GAME`, `TEMPAT_ACARA`, `PENYEDIA`, `TELP_DIHUBUNGI`, `PERATURAN`, `GAMBAR`, `CREATED_AT`) VALUES
('EVNT-AZFZXSX8Z2FKGUX', 'FREE FIRE (MY REPUBLIC) TURNAMENT', '2023-08-02', '16:00:00', '17:00:00', 'FREE FIRE', 'Jl. Keputih, No.43, Kafe Djaja, Sukolilo, Kota Surabaya', 'PT. Sinar Mas', '082663526252', '<h2 style=\"text-align: center; \" segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;=\"\" margin-left:=\"\" 25px;\"=\"\"><span style=\"font-weight: bolder;\">PERATURAN PERTANDINGAN FREE FIRE</span></h2><h2 style=\"font-family: Nunito, \" segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;\"=\"\"><span style=\"font-weight: bolder;\"><br></span></h2><div><span style=\"font-size: 11pt; font-weight: bold;\">1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Persyaratan Pemain</span></div><div>    <span style=\"font-size: 12pt;\">1.1. Setiap pemain wajib memiliki device berupa smartphone serta akun Mobile Legends </span><span style=\"font-size: 12pt;\">sendiri dan bukan milik orang lain.</span></div><div>    <span style=\"font-size: 12pt;\">1.2. Pemain dibawah usia 18 tahun harus mendapatkan persetujuan dari orang tua untuk </span><span style=\"font-size: 12pt;\">berpartisipasi dalam turnamen ini.</span></div><div>    <span style=\"font-size: 12pt;\">1.3 Pemain tidak diperbolehkan untuk memiliki lebih dari 1 tim.</span></div><div>    <span style=\"font-size: 12pt;\">1.4 Pemain wajib hadir di jadwal tanding yang sudah ditentukan (bila melewati batas </span><span style=\"font-size: 12pt;\">tunggu 10 menit, tim akan di diskualifikasi)</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">2. Persyaratan Tim</span></div><div>    <span style=\"font-size: 12pt;\">2.1. Dalam 1 tim wajib menyertakan 1 pemain yang statusnya sudah berlangganan </span><span style=\"font-size: 12pt;\">internet MyRepublic sebelumnya.</span></div><div>    <span style=\"font-size: 12pt;\">2.2. 1 tim beranggotakan 5 pemain inti (wajib) dan 1 pemain cadangan (opsional).</span></div><div>    <span style=\"font-size: 12pt;\">2.3 Menunjuk 1 orang untuk menjadi kapten tim dan kapten tim wajib memiliki WA. </span><span style=\"font-size: 12pt;\">Panitia akan mengirimkan semua pemberitahuan kompetisi melalui WA grup yang </span><span style=\"font-size: 12pt;\">dibuat.</span></div><div>    <span style=\"font-size: 12pt;\">2.4 Tim dilarang mengganti pemain yang sudah terdaftar termasuk pemain cadangan </span><span style=\"font-size: 12pt;\">selama acara berlangsung.</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">3. Nama Tim dan Nama Pemain</span></div><div>    <span style=\"font-size: 12pt;\">3.1. Penggunaan nama pemain atau tim yang mengandung unsur SARA & seksualitas </span><span style=\"font-size: 12pt;\">akan berakibat tim di diskualifikasi oleh pihak panitia.</span></div><div>    <span style=\"font-size: 12pt;\">3.2. 1 tim yang didaftarkan tidak dapat diubah (roster lock)</span></div><div><br></div><div><div><span style=\"font-size: 12pt; font-weight: bold;\">4. Format Mabar from Home</span></div><div>    <span style=\"font-size: 12pt;\">4.1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Turnamen</span></div><div>        <span style=\"font-size: 12pt;\">4.1.1 Sistem pertandingan pada babak penyisihan hingga perempat final </span><span style=\"font-size: 12pt;\">menggunakan sistem single </span><span style=\"font-size: 12pt; font-style: italic;\">elimination (BO1)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.2 Sistem pertandingan pada babak semifinal dan final menggunakan sistem </span><span style=\"font-size: 12pt; font-style: italic;\">best of three (BO3)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.3 Tim akan di undi secara acak untuk menentukan posisi bracket dan </span><span style=\"font-size: 12pt;\">pertandingan akan berlanjut berdasarkan susunan bracket. </span></div></div>', '16907337281690733728_57059406127bcd40da96.jpg', '2023-07-30 16:15:28'),
('EVNT-IBMMTMJWUZK7WUJ', 'LINI POINT TOURNAMENT (FREE FIRE)', '2023-09-08', '14:40:00', '16:40:00', 'FREE FIRE', 'Jl. Tenis, No.43, Cafe Pondok Jus, Sukun, Kota Malang', 'Yamisok.com', '0823372627263', '<h2 style=\"text-align: center; \" segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;=\"\" margin-left:=\"\" 25px;\"=\"\"><span style=\"font-weight: bolder;\">PERATURAN PERTANDINGAN FREE FIRE</span></h2><h2 style=\"font-family: Nunito, \" segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;\"=\"\"><span style=\"font-weight: bolder;\"><br></span></h2><div><span style=\"font-size: 11pt; font-weight: bold;\">1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Persyaratan Pemain</span></div><div>    <span style=\"font-size: 12pt;\">1.1. Setiap pemain wajib memiliki device berupa smartphone serta akun Mobile Legends </span><span style=\"font-size: 12pt;\">sendiri dan bukan milik orang lain.</span></div><div>    <span style=\"font-size: 12pt;\">1.2. Pemain dibawah usia 18 tahun harus mendapatkan persetujuan dari orang tua untuk </span><span style=\"font-size: 12pt;\">berpartisipasi dalam turnamen ini.</span></div><div>    <span style=\"font-size: 12pt;\">1.3 Pemain tidak diperbolehkan untuk memiliki lebih dari 1 tim.</span></div><div>    <span style=\"font-size: 12pt;\">1.4 Pemain wajib hadir di jadwal tanding yang sudah ditentukan (bila melewati batas </span><span style=\"font-size: 12pt;\">tunggu 10 menit, tim akan di diskualifikasi)</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">2. Persyaratan Tim</span></div><div>    <span style=\"font-size: 12pt;\">2.1. Dalam 1 tim wajib menyertakan 1 pemain yang statusnya sudah berlangganan </span><span style=\"font-size: 12pt;\">internet MyRepublic sebelumnya.</span></div><div>    <span style=\"font-size: 12pt;\">2.2. 1 tim beranggotakan 5 pemain inti (wajib) dan 1 pemain cadangan (opsional).</span></div><div>    <span style=\"font-size: 12pt;\">2.3 Menunjuk 1 orang untuk menjadi kapten tim dan kapten tim wajib memiliki WA. </span><span style=\"font-size: 12pt;\">Panitia akan mengirimkan semua pemberitahuan kompetisi melalui WA grup yang </span><span style=\"font-size: 12pt;\">dibuat.</span></div><div>    <span style=\"font-size: 12pt;\">2.4 Tim dilarang mengganti pemain yang sudah terdaftar termasuk pemain cadangan </span><span style=\"font-size: 12pt;\">selama acara berlangsung.</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">3. Nama Tim dan Nama Pemain</span></div><div>    <span style=\"font-size: 12pt;\">3.1. Penggunaan nama pemain atau tim yang mengandung unsur SARA & seksualitas </span><span style=\"font-size: 12pt;\">akan berakibat tim di diskualifikasi oleh pihak panitia.</span></div><div>    <span style=\"font-size: 12pt;\">3.2. 1 tim yang didaftarkan tidak dapat diubah (roster lock)</span></div><div><br></div><div><div><span style=\"font-size: 12pt; font-weight: bold;\">4. Format Mabar from Home</span></div><div>    <span style=\"font-size: 12pt;\">4.1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Turnamen</span></div><div>        <span style=\"font-size: 12pt;\">4.1.1 Sistem pertandingan pada babak penyisihan hingga perempat final </span><span style=\"font-size: 12pt;\">menggunakan sistem single </span><span style=\"font-size: 12pt; font-style: italic;\">elimination (BO1)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.2 Sistem pertandingan pada babak semifinal dan final menggunakan sistem </span><span style=\"font-size: 12pt; font-style: italic;\">best of three (BO3)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.3 Tim akan di undi secara acak untuk menentukan posisi bracket dan </span><span style=\"font-size: 12pt;\">pertandingan akan berlanjut berdasarkan susunan bracket. </span></div></div>', '16914806271691480627_306500743a1a5a56482f.jpg', '2023-08-08 07:43:47'),
('EVNT-LSJVTB6FT9ZMUIJ', 'MOBILE LEGEND (MY REPUBLIC) TURNAMENT', '2023-08-08', '14:30:00', '23:59:00', 'MOBILE LEGEND', 'Jl. Tidar, No. 33, Cafe John, Sukun, Kota Malang', 'PT. Sinar Mas', '089773636273', '<h2 style=\"text-align: left; margin-left: 25px;\"><b>PERATURAN PERTANDINGAN MOBILE LEGEND</b></h2><h2 style=\"text-align: center; \"><b><br></b></h2><div><span style=\"font-size: 11pt; font-weight: bold;\">1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Persyaratan Pemain\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">1.1. Setiap pemain wajib memiliki device berupa smartphone serta akun Mobile Legends&nbsp;</span><span style=\"font-size: 12pt;\">sendiri dan bukan milik orang lain.</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">1.2. Pemain dibawah usia 18 tahun harus mendapatkan persetujuan dari orang tua untuk&nbsp;</span><span style=\"font-size: 12pt;\">berpartisipasi dalam turnamen ini.</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">1.3 Pemain tidak diperbolehkan untuk memiliki lebih dari 1 tim.\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">1.4 Pemain wajib hadir di jadwal tanding yang sudah ditentukan (bila melewati batas&nbsp;</span><span style=\"font-size: 12pt;\">tunggu 10 menit, tim akan di diskualifikasi)</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">2. Persyaratan Tim\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">2.1. Dalam 1 tim wajib menyertakan 1 pemain yang statusnya sudah berlangganan&nbsp;</span><span style=\"font-size: 12pt;\">internet MyRepublic sebelumnya.</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">2.2. 1 tim beranggotakan 5 pemain inti (wajib) dan 1 pemain cadangan (opsional).\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">2.3 Menunjuk 1 orang untuk menjadi kapten tim dan kapten tim wajib memiliki WA.&nbsp;</span><span style=\"font-size: 12pt;\">Panitia akan mengirimkan semua pemberitahuan kompetisi melalui WA grup yang&nbsp;</span><span style=\"font-size: 12pt;\">dibuat.</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">2.4 Tim dilarang mengganti pemain yang sudah terdaftar termasuk pemain cadangan&nbsp;</span><span style=\"font-size: 12pt;\">selama acara berlangsung.</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">3. Nama Tim dan Nama Pemain\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">3.1. Penggunaan nama pemain atau tim yang mengandung unsur SARA &amp; seksualitas&nbsp;</span><span style=\"font-size: 12pt;\">akan berakibat tim di diskualifikasi oleh pihak panitia.</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">3.2. 1 tim yang didaftarkan tidak dapat diubah (roster lock)</span></div><div><br></div><div><!--StartFragment-->\r\n<div><span style=\"font-size: 12pt; font-weight: bold;\">4. Format Mabar from Home\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">4.1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Turnamen\r\n</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">4.1.1 Sistem pertandingan pada babak penyisihan hingga perempat final&nbsp;</span><span style=\"font-size: 12pt;\">menggunakan sistem single </span><span style=\"font-size: 12pt; font-style: italic;\">elimination (BO1)</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">4.1.2 Sistem pertandingan pada babak semifinal dan final menggunakan sistem&nbsp;</span><span style=\"font-size: 12pt; font-style: italic;\">best of three (BO3)</span></div><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-size: 12pt;\">4.1.3 Tim akan di undi secara acak untuk menentukan posisi bracket dan&nbsp;</span><span style=\"font-size: 12pt;\">pertandingan akan berlanjut berdasarkan susunan bracket.&nbsp;</span></div>\r\n<!--EndFragment--></div>', '16906246191690624619_43ade90bc134e2bf55e1.jpg', '2023-07-29 09:56:59'),
('EVNT-MNBSFL7GFHUOJ3C', 'METACO ONLINE TOURNAMENT (PUBG MOBILE)', '2023-09-13', '14:50:00', '22:00:00', 'PUBG MOBILE', 'Pakuwon Mall, Rungkut Madya, Sukolilo, Kota Surabaya', 'Metaco.gg', '082333826883', '<h2 segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;=\"\" margin-left:=\"\" 25px;\"=\"\" style=\"text-align: center; \" center;\"=\"\"><span style=\"font-weight: bolder;\">PERATURAN PERTANDINGAN PUBG MOBILE</span></h2><h2 segoe=\"\" ui\",=\"\" arial;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" text-align:=\"\" center;\"=\"\" style=\"font-family: Nunito, \" 0);\"=\"\"><span style=\"font-weight: bolder;\"><br></span></h2><div><span style=\"font-size: 11pt; font-weight: bold;\">1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Persyaratan Pemain</span></div><div>    <span style=\"font-size: 12pt;\">1.1. Setiap pemain wajib memiliki device berupa smartphone serta akun Mobile Legends </span><span style=\"font-size: 12pt;\">sendiri dan bukan milik orang lain.</span></div><div>    <span style=\"font-size: 12pt;\">1.2. Pemain dibawah usia 18 tahun harus mendapatkan persetujuan dari orang tua untuk </span><span style=\"font-size: 12pt;\">berpartisipasi dalam turnamen ini.</span></div><div>    <span style=\"font-size: 12pt;\">1.3 Pemain tidak diperbolehkan untuk memiliki lebih dari 1 tim.</span></div><div>    <span style=\"font-size: 12pt;\">1.4 Pemain wajib hadir di jadwal tanding yang sudah ditentukan (bila melewati batas </span><span style=\"font-size: 12pt;\">tunggu 10 menit, tim akan di diskualifikasi)</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">2. Persyaratan Tim</span></div><div>    <span style=\"font-size: 12pt;\">2.1. Dalam 1 tim wajib menyertakan 1 pemain yang statusnya sudah berlangganan </span><span style=\"font-size: 12pt;\">internet MyRepublic sebelumnya.</span></div><div>    <span style=\"font-size: 12pt;\">2.2. 1 tim beranggotakan 5 pemain inti (wajib) dan 1 pemain cadangan (opsional).</span></div><div>    <span style=\"font-size: 12pt;\">2.3 Menunjuk 1 orang untuk menjadi kapten tim dan kapten tim wajib memiliki WA. </span><span style=\"font-size: 12pt;\">Panitia akan mengirimkan semua pemberitahuan kompetisi melalui WA grup yang </span><span style=\"font-size: 12pt;\">dibuat.</span></div><div>    <span style=\"font-size: 12pt;\">2.4 Tim dilarang mengganti pemain yang sudah terdaftar termasuk pemain cadangan </span><span style=\"font-size: 12pt;\">selama acara berlangsung.</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">3. Nama Tim dan Nama Pemain</span></div><div>    <span style=\"font-size: 12pt;\">3.1. Penggunaan nama pemain atau tim yang mengandung unsur SARA & seksualitas </span><span style=\"font-size: 12pt;\">akan berakibat tim di diskualifikasi oleh pihak panitia.</span></div><div>    <span style=\"font-size: 12pt;\">3.2. 1 tim yang didaftarkan tidak dapat diubah (roster lock)</span></div><div><br></div><div><div><span style=\"font-size: 12pt; font-weight: bold;\">4. Format Mabar from Home</span></div><div>    <span style=\"font-size: 12pt;\">4.1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Turnamen</span></div><div>        <span style=\"font-size: 12pt;\">4.1.1 Sistem pertandingan pada babak penyisihan hingga perempat final </span><span style=\"font-size: 12pt;\">menggunakan sistem single </span><span style=\"font-size: 12pt; font-style: italic;\">elimination (BO1)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.2 Sistem pertandingan pada babak semifinal dan final menggunakan sistem </span><span style=\"font-size: 12pt; font-style: italic;\">best of three (BO3)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.3 Tim akan di undi secara acak untuk menentukan posisi bracket dan </span><span style=\"font-size: 12pt;\">pertandingan akan berlanjut berdasarkan susunan bracket. </span></div></div>', '16914811231691481123_e18db87b8a61c03fa2cc.jpg', '2023-08-08 07:52:03'),
('EVNT-RFDBNGD0HIPANGX', 'GENSHIN IMPACT (MY REPUBLIC) TURNAMENT', '2023-07-30', '23:39:00', '23:43:00', 'GENSHIN IMPACT', 'Jl. Diponegoro, No.50, Kafe Alvin, Lowokwaru, Kota Malang', 'PT. Sinar Mas', '082772627262', '<h2 style=\"font-family: Nunito, \"Segoe UI\", arial; color: rgb(0, 0, 0); text-align: center; margin-left: 25px;\"><span style=\"font-weight: bolder;\">PERATURAN PERTANDINGAN GENSHIN IMPACT</span></h2><h2 style=\"font-family: Nunito, \"Segoe UI\", arial; color: rgb(0, 0, 0); text-align: center;\"><span style=\"font-weight: bolder;\"><br></span></h2><div><span style=\"font-size: 11pt; font-weight: bold;\">1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Persyaratan Pemain</span></div><div>    <span style=\"font-size: 12pt;\">1.1. Setiap pemain wajib memiliki device berupa smartphone serta akun Mobile Legends </span><span style=\"font-size: 12pt;\">sendiri dan bukan milik orang lain.</span></div><div>    <span style=\"font-size: 12pt;\">1.2. Pemain dibawah usia 18 tahun harus mendapatkan persetujuan dari orang tua untuk </span><span style=\"font-size: 12pt;\">berpartisipasi dalam turnamen ini.</span></div><div>    <span style=\"font-size: 12pt;\">1.3 Pemain tidak diperbolehkan untuk memiliki lebih dari 1 tim.</span></div><div>    <span style=\"font-size: 12pt;\">1.4 Pemain wajib hadir di jadwal tanding yang sudah ditentukan (bila melewati batas </span><span style=\"font-size: 12pt;\">tunggu 10 menit, tim akan di diskualifikasi)</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">2. Persyaratan Tim</span></div><div>    <span style=\"font-size: 12pt;\">2.1. Dalam 1 tim wajib menyertakan 1 pemain yang statusnya sudah berlangganan </span><span style=\"font-size: 12pt;\">internet MyRepublic sebelumnya.</span></div><div>    <span style=\"font-size: 12pt;\">2.2. 1 tim beranggotakan 5 pemain inti (wajib) dan 1 pemain cadangan (opsional).</span></div><div>    <span style=\"font-size: 12pt;\">2.3 Menunjuk 1 orang untuk menjadi kapten tim dan kapten tim wajib memiliki WA. </span><span style=\"font-size: 12pt;\">Panitia akan mengirimkan semua pemberitahuan kompetisi melalui WA grup yang </span><span style=\"font-size: 12pt;\">dibuat.</span></div><div>    <span style=\"font-size: 12pt;\">2.4 Tim dilarang mengganti pemain yang sudah terdaftar termasuk pemain cadangan </span><span style=\"font-size: 12pt;\">selama acara berlangsung.</span></div><div><span style=\"font-size: 12pt;\"><br></span></div><div><span style=\"font-size: 12pt; font-weight: bold;\">3. Nama Tim dan Nama Pemain</span></div><div>    <span style=\"font-size: 12pt;\">3.1. Penggunaan nama pemain atau tim yang mengandung unsur SARA & seksualitas </span><span style=\"font-size: 12pt;\">akan berakibat tim di diskualifikasi oleh pihak panitia.</span></div><div>    <span style=\"font-size: 12pt;\">3.2. 1 tim yang didaftarkan tidak dapat diubah (roster lock)</span></div><div><br></div><div><div><span style=\"font-size: 12pt; font-weight: bold;\">4. Format Mabar from Home</span></div><div>    <span style=\"font-size: 12pt;\">4.1. </span><span style=\"font-size: 12pt; font-weight: bold;\">Turnamen</span></div><div>        <span style=\"font-size: 12pt;\">4.1.1 Sistem pertandingan pada babak penyisihan hingga perempat final </span><span style=\"font-size: 12pt;\">menggunakan sistem single </span><span style=\"font-size: 12pt; font-style: italic;\">elimination (BO1)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.2 Sistem pertandingan pada babak semifinal dan final menggunakan sistem </span><span style=\"font-size: 12pt; font-style: italic;\">best of three (BO3)</span></div><div>        <span style=\"font-size: 12pt;\">4.1.3 Tim akan di undi secara acak untuk menentukan posisi bracket dan </span><span style=\"font-size: 12pt;\">pertandingan akan berlanjut berdasarkan susunan bracket. </span></div></div>', '16907340751690734075_3db8320b985807057fa5.jpg', '2023-07-30 16:21:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `FRIENDS`
--

CREATE TABLE `FRIENDS` (
  `ID_USER_ME` char(20) NOT NULL,
  `ID_USER_FRIEND` char(20) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `FRIENDS`
--

INSERT INTO `FRIENDS` (`ID_USER_ME`, `ID_USER_FRIEND`, `CREATED_AT`) VALUES
('USRS-T5BIWEOWTB1WEJP', 'USRS-21XRJBRHXLU5H11', '2023-10-18 12:54:26'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-1TXMVVUZU7E9B8A', '2023-10-18 12:54:40'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-27QELP0JIRGFMVA', '2023-10-18 12:54:45'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-3GDRLSN7HOKVC2E', '2023-10-18 12:54:50'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-3VOIUAZRQM5D7SQ', '2023-10-18 12:54:55'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-45APCIKVCYS2PTL', '2023-10-18 12:55:02'),
('USRS-T5BIWEOWTB1WEJP', 'USRS-4EY58QCE98HM3NL', '2023-10-18 12:55:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ID_GAMES`
--

CREATE TABLE `ID_GAMES` (
  `ID_IDGAME` char(20) NOT NULL,
  `JENIS_GAME` enum('MOBILE LEGENDS','PUBG MOBILE','FREE FIRE','GENSHIN IMPACT','CLASH OF CLANS') NOT NULL,
  `ID_GAME` char(20) NOT NULL,
  `RANKED` varchar(150) NOT NULL,
  `SCREENSHOOT_1` varchar(85) DEFAULT NULL,
  `SCREENSHOOT_2` varchar(85) DEFAULT NULL,
  `SCREENSHOOT_3` varchar(85) DEFAULT NULL,
  `ID_USER` char(20) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `ID_GAMES`
--

INSERT INTO `ID_GAMES` (`ID_IDGAME`, `JENIS_GAME`, `ID_GAME`, `RANKED`, `SCREENSHOOT_1`, `SCREENSHOOT_2`, `SCREENSHOOT_3`, `ID_USER`, `CREATED_AT`) VALUES
('IDGM-00IMSYCRC4W03CS', 'FREE FIRE', '2136625', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BU3IRXEITHMP6U3', '2023-08-16 15:25:51'),
('IDGM-07AG6IJRUJPPNUW', 'FREE FIRE', '5343332', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZNJERALDVNLNIPG', '2023-08-16 15:25:51'),
('IDGM-08HS7OGJHESOR3B', 'CLASH OF CLANS', '3322164', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BU3IRXEITHMP6U3', '2023-08-16 15:25:51'),
('IDGM-0AM7FGXJ2BAGMBL', 'PUBG MOBILE', '9589124', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-0CXXEVQAKCDFB2V', 'GENSHIN IMPACT', '7749792', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UFSTAUVGXDO7HYN', '2023-08-16 15:25:51'),
('IDGM-0KAI07ZULJ1VLES', 'CLASH OF CLANS', '9715957', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('IDGM-0LKFNBDJAJ2QNAI', 'MOBILE LEGENDS', '3664865', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IF1LVNXVJI6MNJC', '2023-08-16 15:25:51'),
('IDGM-0TQMBY9V3WVEUGI', 'MOBILE LEGENDS', '2866137', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GTYOM1CKSUZU1ZY', '2023-08-16 15:25:51'),
('IDGM-0WDMQOD8MZMQHLW', 'PUBG MOBILE', '9253473', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('IDGM-1C60HINCLUVFHXW', 'GENSHIN IMPACT', '6425149', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LOLWQQK4INFH3QT', '2023-08-16 15:25:51'),
('IDGM-1CFDACRWZQEQU4M', 'FREE FIRE', '5983588', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-1CO4A3JUUJSM5YR', 'MOBILE LEGENDS', '1799145', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-R8ZHQJWOUMFAFCD', '2023-08-16 15:25:51'),
('IDGM-1JTFIYVBCVKE6EL', 'GENSHIN IMPACT', '7825699', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-8YFANWOOPQ60QET', '2023-08-16 15:25:51'),
('IDGM-2MWICJDAYDFI0LV', 'FREE FIRE', '2651575', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-VKUS2BUUPXNIEMZ', '2023-08-16 15:25:51'),
('IDGM-2NSORI7FW11HRGS', 'GENSHIN IMPACT', '6692195', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZEB9WQWXA7M81CK', '2023-08-16 15:25:51'),
('IDGM-31EHR8XDLHW6BUW', 'PUBG MOBILE', '7179546', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IDEG6LS7E5CJTHA', '2023-08-16 15:25:51'),
('IDGM-32PQRPNWVCSJNFZ', 'MOBILE LEGENDS', '1182828', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-S9ZJGKCUHBQPDBP', '2023-08-16 15:25:51'),
('IDGM-39VFH6IIEV54JN2', 'CLASH OF CLANS', '9438321', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UFSTAUVGXDO7HYN', '2023-08-16 15:25:51'),
('IDGM-3DWSG2GYHWSG9DB', 'FREE FIRE', '3957232', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-3NK1N8YFMVJV89W', 'MOBILE LEGENDS', '5392662', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('IDGM-3OVB1XAFJVDJ4MT', 'CLASH OF CLANS', '1411141', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DURPMA7MHMQBMBG', '2023-08-16 15:25:51'),
('IDGM-43AJYGXSPAS2HGZ', 'CLASH OF CLANS', '2622676', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-54EATO6DBBJCGRG', '2023-08-16 15:25:51'),
('IDGM-4DLH0MOYCJFXICI', 'CLASH OF CLANS', '4837254', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-OJ1ODV7DLF6USHE', '2023-08-16 15:25:51'),
('IDGM-4FCMY8T8JIRXERH', 'CLASH OF CLANS', '3452934', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('IDGM-4FSJQBTWTM7C4ZH', 'FREE FIRE', '2145763', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-4GUTWS7LTXMHLG4', 'CLASH OF CLANS', '9226471', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NMEKYNSTLTFW4JK', '2023-08-16 15:25:51'),
('IDGM-52N6AJEXTMJPY4Y', 'MOBILE LEGENDS', '5986381', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NMEKYNSTLTFW4JK', '2023-08-16 15:25:51'),
('IDGM-542C4P2ZLWU1SXW', 'MOBILE LEGENDS', '1622788', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-21XRJBRHXLU5H11', '2023-08-16 15:25:51'),
('IDGM-5ASX8BEWKPSXMMW', 'GENSHIN IMPACT', '7946351', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JYGOHJHVUWKWGBL', '2023-08-16 15:25:51'),
('IDGM-5CKURRID9H3NCYT', 'MOBILE LEGENDS', '4544486', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IIOSZF73JJFQ6TW', '2023-08-16 15:25:51'),
('IDGM-5I2KRHEGYZKXOON', 'MOBILE LEGENDS', '1224819', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BAABXXSPS3MY5B1', '2023-08-16 15:25:51'),
('IDGM-5KJTRZ9XETFYTL9', 'MOBILE LEGENDS', '5788928', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KU23AWTK1KT9IB4', '2023-08-16 15:25:51'),
('IDGM-5KQVLQYUZCLKUOQ', 'CLASH OF CLANS', '4553576', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('IDGM-5OG0XTQZ7RBALAV', 'PUBG MOBILE', '5588921', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZY1ZNAZPQPBKRQ3', '2023-08-16 15:25:51'),
('IDGM-5RWUUXBLAGZJTWP', 'GENSHIN IMPACT', '7826612', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DLC4AQSJ0MSMLIG', '2023-08-16 15:25:51'),
('IDGM-63LUMFUENR8LEIJ', 'PUBG MOBILE', '8729165', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TELYXBMV55K5RBJ', '2023-08-16 15:25:51'),
('IDGM-660C2HTMLEJI8LN', 'GENSHIN IMPACT', '9821967', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('IDGM-67S9NALIRHWXJCJ', 'MOBILE LEGENDS', '5639176', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CM1O0VLGKLBCW92', '2023-08-16 15:25:51'),
('IDGM-6JEREYRBSYRXYXE', 'PUBG MOBILE', '4364318', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('IDGM-6M3MZY97ZLWF8BS', 'CLASH OF CLANS', '9661746', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-X7P96132YBENCLY', '2023-08-16 15:25:51'),
('IDGM-6RNGEBUI1LAJXFX', 'CLASH OF CLANS', '7154126', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-R4T2QOEBZJFJLZD', '2023-08-16 15:25:51'),
('IDGM-7MWNJDCKHGDYX6K', 'FREE FIRE', '3662682', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('IDGM-7U8QXVVARCWJQ0S', 'CLASH OF CLANS', '3584772', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RWQKQKONKLQARWJ', '2023-08-16 15:25:51'),
('IDGM-7UO7VR5KMZLLDRB', 'PUBG MOBILE', '1186651', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZEB9WQWXA7M81CK', '2023-08-16 15:25:51'),
('IDGM-90DMBFL5UBNUEQP', 'CLASH OF CLANS', '7968138', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('IDGM-9C1JBS93SEUBDNJ', 'CLASH OF CLANS', '2121612', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SIBGYKYHXYOUMKI', '2023-08-16 15:25:51'),
('IDGM-9SWAEYTJ0NNHUJT', 'MOBILE LEGENDS', '8678869', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('IDGM-9WPSRHVAGGAINHO', 'FREE FIRE', '6393494', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-B5HY9ETVPIF2X8F', '2023-08-16 15:25:51'),
('IDGM-9YT7IQMVRUSQGXC', 'CLASH OF CLANS', '9573728', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-A7SAD9IIKO3MIM0', 'GENSHIN IMPACT', '1234179', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-S9ZJGKCUHBQPDBP', '2023-08-16 15:25:51'),
('IDGM-ABPLFQFSIUUA75M', 'FREE FIRE', '7357119', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-ACYMTUWXTSU04XA', 'PUBG MOBILE', '4835542', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-AD2AAWKNVTVLXWD', 'CLASH OF CLANS', '2963911', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GKFVZMBQMHXQEPO', '2023-08-16 15:25:51'),
('IDGM-ALRUZ0VGCACJCVO', 'GENSHIN IMPACT', '1414957', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RWQKQKONKLQARWJ', '2023-08-16 15:25:51'),
('IDGM-ALTVUSZPWHMY5TJ', 'FREE FIRE', '7351855', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('IDGM-AUD02KYDTEPEGIN', 'GENSHIN IMPACT', '2556277', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ML70PYUK8YMF7IQ', '2023-08-16 15:25:51'),
('IDGM-AYXC33CKPZYOQNO', 'CLASH OF CLANS', '2535762', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-B5HY9ETVPIF2X8F', '2023-08-16 15:25:51'),
('IDGM-AZ08TMMVER3TNOX', 'MOBILE LEGENDS', '2697685', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZKITBG6QVJPHCJC', '2023-08-16 15:25:51'),
('IDGM-AZGT7CKUDCZJDTU', 'GENSHIN IMPACT', '4913765', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LOLWQQK4INFH3QT', '2023-08-16 15:25:51'),
('IDGM-AZVY8OGUUGSHMRM', 'GENSHIN IMPACT', '771818', 'Test', NULL, NULL, NULL, 'IDGM-2HCOEHQXNRMBPJJ', '2023-08-14 19:48:06'),
('IDGM-B6ZG9FDRYNJIDLP', 'MOBILE LEGENDS', '6189198', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XPIZMO5FK8CFRT3', '2023-08-16 15:25:51'),
('IDGM-B7QTEEJRP9QGELX', 'PUBG MOBILE', '4448292', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CM1O0VLGKLBCW92', '2023-08-16 15:25:51'),
('IDGM-BASPX3MLIFZIV3B', 'FREE FIRE', '3764553', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IF1LVNXVJI6MNJC', '2023-08-16 15:25:51'),
('IDGM-BMJRVAEZPKLSBAN', 'CLASH OF CLANS', '2944527', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-8YFANWOOPQ60QET', '2023-08-16 15:25:51'),
('IDGM-BOIAII1SFI92VBA', 'PUBG MOBILE', '2684415', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-R8ZHQJWOUMFAFCD', '2023-08-16 15:25:51'),
('IDGM-BRPMPPUKEZTJNTK', 'PUBG MOBILE', '6876837', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EDM6TV3S11D9IAS', '2023-08-16 15:25:51'),
('IDGM-BTIG7NUK00G69ZJ', 'CLASH OF CLANS', '1441198', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('IDGM-BUBJSSV6AEYZXZY', 'PUBG MOBILE', '7841814', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-WF91IBUZIQXYJDH', '2023-08-16 15:25:51'),
('IDGM-BYNCSCCX2PTVHVW', 'GENSHIN IMPACT', '1295175', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W84IL6WOLXZTWHF', '2023-08-16 15:25:51'),
('IDGM-C1T1POTKLVEX05E', 'GENSHIN IMPACT', '7517361', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-PR6SSGMRFR3IQWK', '2023-08-16 15:25:51'),
('IDGM-C8ZWY59UY9LV39N', 'MOBILE LEGENDS', '4172111', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-4EY58QCE98HM3NL', '2023-08-16 15:25:51'),
('IDGM-C9OUP3KCHCMHEPI', 'MOBILE LEGENDS', '6313411', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SIBGYKYHXYOUMKI', '2023-08-16 15:25:51'),
('IDGM-CA57JA6JZUAZCEO', 'MOBILE LEGENDS', '9584777', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QZF22QPKLFOCAXY', '2023-08-16 15:25:51'),
('IDGM-CDPX48YSWYPULYD', 'CLASH OF CLANS', '6779436', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SM1DPTXBO0FLQEA', '2023-08-16 15:25:51'),
('IDGM-CHE4ZYWQORGKXG8', 'CLASH OF CLANS', '4765734', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('IDGM-CMTE8M3VEXPSPPK', 'GENSHIN IMPACT', '1289648', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UFSTAUVGXDO7HYN', '2023-08-16 15:25:51'),
('IDGM-CN33U5WU6N8SYNJ', 'FREE FIRE', '77181771', 'Glory', '16920424581692042458_645916f97f3573927d8b.png', NULL, NULL, 'IDGM-O3RPJWWDNKOGLXT', '2023-08-14 19:47:38'),
('IDGM-CP7NF3YNSHZRGXA', 'CLASH OF CLANS', '7789894', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GBGBYZRKMHBLJSO', '2023-08-16 15:25:51'),
('IDGM-CVM6IGKZJ0BYWKD', 'CLASH OF CLANS', '4375323', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZIJ2P6JFGFSMQDR', '2023-08-16 15:25:51'),
('IDGM-CX9S57S0HGVIRH6', 'MOBILE LEGENDS', '7663512', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NMEKYNSTLTFW4JK', '2023-08-16 15:25:51'),
('IDGM-D2JKHMLA9E87BUN', 'CLASH OF CLANS', '3835948', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-D31RRWYL1D1M2IW', 'GENSHIN IMPACT', '4533385', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('IDGM-D3QBPSRUYQB34IF', 'FREE FIRE', '9958419', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U1B8KFHBWZBPIRG', '2023-08-16 15:25:51'),
('IDGM-D4ILOFLS0NSIBBU', 'CLASH OF CLANS', '6759514', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('IDGM-DEEIE8Z8PVQRJKX', 'FREE FIRE', '9663759', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IDEG6LS7E5CJTHA', '2023-08-16 15:25:51'),
('IDGM-DFO4EJPLIJTAZ1S', 'CLASH OF CLANS', '6664722', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-DNBISEWSOJS4LPW', 'GENSHIN IMPACT', '1386317', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('IDGM-DNH8YTOOO03IL50', 'CLASH OF CLANS', '8681844', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-C89TCPVZUDQZ6Y3', '2023-08-16 15:25:51'),
('IDGM-DYDT4BD8Y7NQO2I', 'CLASH OF CLANS', '8732416', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-B5HY9ETVPIF2X8F', '2023-08-16 15:25:51'),
('IDGM-E1IOJBYTP2NP0CX', 'FREE FIRE', '2641258', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('IDGM-E5VG1CEBX4ESCT0', 'CLASH OF CLANS', '1927577', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-VKUS2BUUPXNIEMZ', '2023-08-16 15:25:51'),
('IDGM-EA4LQCTYLE1OVBB', 'CLASH OF CLANS', '3473616', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-1TXMVVUZU7E9B8A', '2023-08-16 15:25:51'),
('IDGM-EFZCDJVSIIF5MVB', 'GENSHIN IMPACT', '8448645', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DLC4AQSJ0MSMLIG', '2023-08-16 15:25:51'),
('IDGM-EGB9AMEEFYCTVEN', 'PUBG MOBILE', '5741522', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UFSTAUVGXDO7HYN', '2023-08-16 15:25:51'),
('IDGM-EJSYP9AVHYZ5QMM', 'MOBILE LEGENDS', '9623965', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HDDY9VYYSAVSK1B', '2023-08-16 15:25:51'),
('IDGM-EMBDVDFC9KVAKE6', 'FREE FIRE', '4291354', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-EQJ8B2VX4HVQF9S', 'FREE FIRE', '6984851', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7PI8XLQZ0TKMONS', '2023-08-16 15:25:51'),
('IDGM-EQTHEWJZYAB5L8X', 'FREE FIRE', '8851373', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('IDGM-ETI4QRUYR9ECZIR', 'GENSHIN IMPACT', '6817822', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JYGOHJHVUWKWGBL', '2023-08-16 15:25:51'),
('IDGM-EUXZRF3KYY159KS', 'PUBG MOBILE', '5624474', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XSB8Q9RTSOLQUGW', '2023-08-16 15:25:51'),
('IDGM-EUYLZZ7BNTIUX5I', 'MOBILE LEGENDS', '7763524', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('IDGM-F9RJ547H8L07GPB', 'FREE FIRE', '9295127', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-X7P96132YBENCLY', '2023-08-16 15:25:51'),
('IDGM-FF57WEXSIPHTGAA', 'CLASH OF CLANS', '7263589', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-FFVYAKEZ9IZXKQX', 'GENSHIN IMPACT', '1827341', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-Q9DAMYWHNYBLQ2A', '2023-08-16 15:25:51'),
('IDGM-FLDDQSU32GHET39', 'MOBILE LEGENDS', '4176357', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-Q9DAMYWHNYBLQ2A', '2023-08-16 15:25:51'),
('IDGM-FOPHM7KAPOONQNA', 'PUBG MOBILE', '9738722', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FCIOSOV0Z7JTGAL', '2023-08-16 15:25:51'),
('IDGM-FTE2CPEXLPNRIH7', 'MOBILE LEGENDS', '6146472', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-WF91IBUZIQXYJDH', '2023-08-16 15:25:51'),
('IDGM-FTSWK5JYECV1ATE', 'FREE FIRE', '9887921', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZEB9WQWXA7M81CK', '2023-08-16 15:25:51'),
('IDGM-FUGMTZSBJWUDFDM', 'FREE FIRE', '2697268', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GDLUX5L9H6PT7M2', '2023-08-16 15:25:51'),
('IDGM-FWK43UVKQNIL2TB', 'PUBG MOBILE', '8935538', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GDLUX5L9H6PT7M2', '2023-08-16 15:25:51'),
('IDGM-FYTTJNURUX3YSC1', 'PUBG MOBILE', '2286689', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BAABXXSPS3MY5B1', '2023-08-16 15:25:51'),
('IDGM-FZYW14GBXP0Q6XI', 'PUBG MOBILE', '2323566', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-G3RSNJTUCAXMSBG', 'FREE FIRE', '1456183', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('IDGM-G4E2MG2KHMOIHNZ', 'PUBG MOBILE', '1939555', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AIBLPPSMTCRK06A', '2023-08-16 15:25:51'),
('IDGM-GBJCROHVAFKJPRF', 'FREE FIRE', '3698589', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-K8QOAMBGQZPJFSN', '2023-08-16 15:25:51'),
('IDGM-GGWSJOXRYULDD4D', 'GENSHIN IMPACT', '1561526', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('IDGM-GHWVI9LH99VO7F7', 'MOBILE LEGENDS', '6272717', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSENDCFVN07T7Y7', '2023-08-16 15:25:51'),
('IDGM-GJ2UNISJKQ8MMWB', 'CLASH OF CLANS', '3516724', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-45APCIKVCYS2PTL', '2023-08-16 15:25:51'),
('IDGM-GKVSLRZKSSEKACB', 'GENSHIN IMPACT', '4419216', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CZZYKMUCFQRYR7N', '2023-08-16 15:25:51'),
('IDGM-GNT9GVB6UJHG0J0', 'MOBILE LEGENDS', '6643634', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FGFBKLWMSS1M03R', '2023-08-16 15:25:51'),
('IDGM-GO7GBIZH2YWM8UF', 'GENSHIN IMPACT', '6585443', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('IDGM-GOP1W27LTMRI9IU', 'PUBG MOBILE', '8259496', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-GV7P3SXTENYFKGV', 'PUBG MOBILE', '1245316', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ML70PYUK8YMF7IQ', '2023-08-16 15:25:51'),
('IDGM-GVJFSOAPEX2KPTJ', 'MOBILE LEGENDS', '8882221', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YEECAXHKXLUK7M4', '2023-08-16 15:25:51'),
('IDGM-GWRTQVGRCJZQCZZ', 'CLASH OF CLANS', '5779789', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-GZ2JZ7IYP8TOYGJ', 'GENSHIN IMPACT', '6848696', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IF1LVNXVJI6MNJC', '2023-08-16 15:25:51'),
('IDGM-GZB5OFHOTAXWQXJ', 'FREE FIRE', '6981248', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-H3Z5BXKZU1J33YT', 'CLASH OF CLANS', '3131199', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-4EY58QCE98HM3NL', '2023-08-16 15:25:51'),
('IDGM-H4PZQG8DYLQMBAY', 'GENSHIN IMPACT', '9717674', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IDEG6LS7E5CJTHA', '2023-08-16 15:25:51'),
('IDGM-H6J7I4QX3VB7BPA', 'GENSHIN IMPACT', '9596346', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W84IL6WOLXZTWHF', '2023-08-16 15:25:51'),
('IDGM-HB243OQVKQBULEX', 'GENSHIN IMPACT', '1995822', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-1TXMVVUZU7E9B8A', '2023-08-16 15:25:51'),
('IDGM-HBE3ZWOY5K1UHFH', 'FREE FIRE', '4419746', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-HBJFYQTET5OUXUI', 'PUBG MOBILE', '5514493', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-R4T2QOEBZJFJLZD', '2023-08-16 15:25:51'),
('IDGM-HDNFIEWM2YS3ZFD', 'FREE FIRE', '6188299', 'Gold', '16928892521692889252_9d050a42611f7f3d52ca.png', NULL, NULL, 'USRS-T5BIWEOWTB1WEJP', '2023-08-24 15:00:52'),
('IDGM-HF0QRN0D64XZ5QC', 'GENSHIN IMPACT', '3882153', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZY1ZNAZPQPBKRQ3', '2023-08-16 15:25:51'),
('IDGM-HF1XCRROBG6SWEN', 'PUBG MOBILE', '6676599', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CBR0IYLZS7UINWG', '2023-08-16 15:25:51'),
('IDGM-HGTPKYUSC4U2L93', 'CLASH OF CLANS', '7248767', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SM1DPTXBO0FLQEA', '2023-08-16 15:25:51'),
('IDGM-HKEIYYCVZHHK1DT', 'GENSHIN IMPACT', '8218159', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7PI8XLQZ0TKMONS', '2023-08-16 15:25:51'),
('IDGM-HLJSQO5IAFJHVK1', 'FREE FIRE', '8682147', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EDM6TV3S11D9IAS', '2023-08-16 15:25:51'),
('IDGM-HLPOIKTMCJBZPQL', 'FREE FIRE', '5652927', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FEZ7IRLU2CWUL75', '2023-08-16 15:25:51'),
('IDGM-HSDZ8UVQ34IJSCI', 'GENSHIN IMPACT', '6974548', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('IDGM-HTQH5NNOUSEMWJL', 'GENSHIN IMPACT', '5524671', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-HWHGZWIW4KMIPY0', 'CLASH OF CLANS', '6267177', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPADIWFZFNBMOWR', '2023-08-16 15:25:51'),
('IDGM-HX3P3WDNYQZJSOP', 'PUBG MOBILE', '3466819', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('IDGM-I17H4UE0JO6R5SJ', 'FREE FIRE', '2533658', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-27QELP0JIRGFMVA', '2023-08-16 15:25:51'),
('IDGM-I1AMROMNT1WNHRX', 'MOBILE LEGENDS', '1573562', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DLC4AQSJ0MSMLIG', '2023-08-16 15:25:51'),
('IDGM-I381JIXA7ILBVBC', 'MOBILE LEGENDS', '5835243', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('IDGM-I9FSIUMP7ZY2PME', 'MOBILE LEGENDS', '4469382', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GTYOM1CKSUZU1ZY', '2023-08-16 15:25:51'),
('IDGM-IB8UPIZCMDIVOJY', 'GENSHIN IMPACT', '1357389', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-66QKYWU46VS2CCC', '2023-08-16 15:25:51'),
('IDGM-IBKCDIMBWFOBAVU', 'MOBILE LEGENDS', '55754', 'Mythic', '16928891971692889197_866732093c88272ea7fd.png', NULL, NULL, 'USRS-T5BIWEOWTB1WEJP', '2023-08-24 14:59:57'),
('IDGM-IMCHT83UHHS7GJD', 'PUBG MOBILE', '9861199', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-54EATO6DBBJCGRG', '2023-08-16 15:25:51'),
('IDGM-IPOAXAVUFAVVNDX', 'FREE FIRE', '5914896', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPADIWFZFNBMOWR', '2023-08-16 15:25:51'),
('IDGM-ISQIMNVOVDODUWQ', 'GENSHIN IMPACT', '1837413', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DLC4AQSJ0MSMLIG', '2023-08-16 15:25:51'),
('IDGM-IWVYB8YGPIG8DAB', 'GENSHIN IMPACT', '3486815', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('IDGM-IZYM0MBYCB1BLLS', 'CLASH OF CLANS', '7315813', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-J3ZRUU43XZCJKK1', 'GENSHIN IMPACT', '8941765', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('IDGM-J6V33JOJ7PBHIMG', 'GENSHIN IMPACT', '8626735', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-8YFANWOOPQ60QET', '2023-08-16 15:25:51'),
('IDGM-JBDILUNNHFQLCB8', 'GENSHIN IMPACT', '771818', 'Test', '16920425561692042556_25a2b6cb40d3e9ef43bd.png', '16920425561692042556_e29c5c5d0da1f2363245.png', NULL, 'IDGM-2HCOEHQXNRMBPJJ', '2023-08-14 19:49:16'),
('IDGM-JBXVW1RTI65PEVJ', 'GENSHIN IMPACT', '9614927', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('IDGM-JEMX8UYXXOJZKEH', 'GENSHIN IMPACT', '3842874', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-66QKYWU46VS2CCC', '2023-08-16 15:25:51'),
('IDGM-JG27TDGCTF1E5DO', 'GENSHIN IMPACT', '7515418', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ALR6WNLRSNEKZAB', '2023-08-16 15:25:51'),
('IDGM-JJKBBEKTRWBE3NI', 'GENSHIN IMPACT', '1971932', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UTCUVBAIO7QOCF7', '2023-08-16 15:25:51'),
('IDGM-JJZOBJDICMYLC4T', 'MOBILE LEGENDS', '5465959', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-JOVHDMVSTCH8KM2', 'MOBILE LEGENDS', '2974498', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('IDGM-JRA0BUAEPZHYI2X', 'MOBILE LEGENDS', '5276696', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-21XRJBRHXLU5H11', '2023-08-16 15:25:51'),
('IDGM-JTKHLPXPCDVQRJ9', 'GENSHIN IMPACT', '9653922', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-S9ZJGKCUHBQPDBP', '2023-08-16 15:25:51'),
('IDGM-JWMRRMKS560VLIH', 'FREE FIRE', '9259236', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GKFVZMBQMHXQEPO', '2023-08-16 15:25:51'),
('IDGM-JXGAPMTY9R8JMHK', 'CLASH OF CLANS', '5314592', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZY1ZNAZPQPBKRQ3', '2023-08-16 15:25:51'),
('IDGM-JYCGNOUDXKLZOBC', 'GENSHIN IMPACT', '9439571', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TELYXBMV55K5RBJ', '2023-08-16 15:25:51'),
('IDGM-KB10EIXEU0PHWWI', 'GENSHIN IMPACT', '3325421', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HUEOW7NXUDXM8HE', '2023-08-16 15:25:51'),
('IDGM-KCRSEXCPKV8RJO9', 'GENSHIN IMPACT', '1975693', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-KDBVP5JUZWYJRUC', 'FREE FIRE', '5953239', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XSB8Q9RTSOLQUGW', '2023-08-16 15:25:51'),
('IDGM-KFDMKC2B9YTQDQI', 'MOBILE LEGENDS', '9512671', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZIJ2P6JFGFSMQDR', '2023-08-16 15:25:51'),
('IDGM-KH7AA3NBRYPOZWR', 'GENSHIN IMPACT', '9455193', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSENDCFVN07T7Y7', '2023-08-16 15:25:51'),
('IDGM-KIDIS4A3C5FOKFG', 'MOBILE LEGENDS', '2726163', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-66QKYWU46VS2CCC', '2023-08-16 15:25:51'),
('IDGM-KJLP29IAVTUTNVC', 'GENSHIN IMPACT', '3974624', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BU3IRXEITHMP6U3', '2023-08-16 15:25:51'),
('IDGM-KLAO0DZTWTVN7ZT', 'GENSHIN IMPACT', '7577152', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QZF22QPKLFOCAXY', '2023-08-16 15:25:51'),
('IDGM-KMLF8ITMOVVNQCJ', 'CLASH OF CLANS', '8361341', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RBFWKQSIBRN998M', '2023-08-16 15:25:51'),
('IDGM-KMOSFMMDRVFRCGG', 'FREE FIRE', '5275495', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GBGBYZRKMHBLJSO', '2023-08-16 15:25:51'),
('IDGM-KPBZ3TO2YKEK1RR', 'FREE FIRE', '4832338', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('IDGM-KPMLVARGQXMOHOS', 'FREE FIRE', '4354697', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('IDGM-KTILFBGV3YWE51J', 'MOBILE LEGENDS', '4126638', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('IDGM-KVGDADKBEQCDFTM', 'CLASH OF CLANS', '4733447', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('IDGM-KWXUSNBGDPVSWY1', 'PUBG MOBILE', '2968532', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-KXMECIBUD4TGIFF', 'PUBG MOBILE', '9313928', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-L0NNIGYIFLDR6TS', 'MOBILE LEGENDS', '6276276', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-L5PLC4HTBSF18LW', 'CLASH OF CLANS', '8341479', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LOLWQQK4INFH3QT', '2023-08-16 15:25:51'),
('IDGM-LCY8A5DOSGYNNAN', 'CLASH OF CLANS', '2878323', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('IDGM-LDMNNGODNZFMOFV', 'GENSHIN IMPACT', '6338366', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XSB8Q9RTSOLQUGW', '2023-08-16 15:25:51'),
('IDGM-LLZSN3FXOICCTU0', 'FREE FIRE', '7114896', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-4EY58QCE98HM3NL', '2023-08-16 15:25:51'),
('IDGM-LQADYWIF8KPNJQI', 'GENSHIN IMPACT', '3485388', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('IDGM-LTNGLDZXUEYC9HA', 'MOBILE LEGENDS', '7168626', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('IDGM-LTUPG9CGPXZC0DX', 'PUBG MOBILE', '6135935', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-X7P96132YBENCLY', '2023-08-16 15:25:51'),
('IDGM-LTYK2Q7G33V9FZG', 'PUBG MOBILE', '4415893', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CM1O0VLGKLBCW92', '2023-08-16 15:25:51'),
('IDGM-LVF1SCBQF9H5TBD', 'FREE FIRE', '1126232', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GBGBYZRKMHBLJSO', '2023-08-16 15:25:51'),
('IDGM-LYJ7S6GGPDC8ULW', 'MOBILE LEGENDS', '3236265', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HUEOW7NXUDXM8HE', '2023-08-16 15:25:51'),
('IDGM-LZ0RA1O1Q5CUEZV', 'PUBG MOBILE', '1268652', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-MIGLGNCHRD8ZLVM', 'GENSHIN IMPACT', '8746689', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FEZ7IRLU2CWUL75', '2023-08-16 15:25:51'),
('IDGM-MKLZW6TZXIUMUU1', 'CLASH OF CLANS', '1647685', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-OJ1ODV7DLF6USHE', '2023-08-16 15:25:51'),
('IDGM-MNCXKIPLAKWSIFS', 'MOBILE LEGENDS', '6872333', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-54EATO6DBBJCGRG', '2023-08-16 15:25:51'),
('IDGM-MOYB9RS4IOWBXSO', 'CLASH OF CLANS', '5716643', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XPIZMO5FK8CFRT3', '2023-08-16 15:25:51'),
('IDGM-MPRE0FOQKVQSDQZ', 'FREE FIRE', '4633714', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('IDGM-MTEBTYDXMOGWXMJ', 'MOBILE LEGENDS', '5475456', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JYGOHJHVUWKWGBL', '2023-08-16 15:25:51'),
('IDGM-MTJ8HSS0DL2W1BF', 'GENSHIN IMPACT', '6793784', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LBKSFXILWRIFW60', '2023-08-16 15:25:51'),
('IDGM-MTKX7ZI9K9NNYZE', 'FREE FIRE', '4924548', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-MXBAJLZHAIDMXQW', 'GENSHIN IMPACT', '5171394', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YENXXSVKJPJ0YAD', '2023-08-16 15:25:51'),
('IDGM-MXDVHSKKP0AEMHQ', 'GENSHIN IMPACT', '3277968', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-N3MEAN0NL4NHAGU', 'MOBILE LEGENDS', '2643982', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SDF4F91WYCLMWVN', '2023-08-16 15:25:51'),
('IDGM-N4EIHXSTCC7DOGD', 'MOBILE LEGENDS', '1292231', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-K3OWW3GSTTHTKFP', '2023-08-16 15:25:51'),
('IDGM-N8HQJXX8BQ9I2G8', 'MOBILE LEGENDS', '2667442', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('IDGM-N8ML3ISRAY40XFL', 'CLASH OF CLANS', '9821594', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UEQ1E3TDPMJNBSH', '2023-08-16 15:25:51'),
('IDGM-NGRUID0ZDE6L5UU', 'CLASH OF CLANS', '8669773', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EWXZDXF8WJLXKCL', '2023-08-16 15:25:51'),
('IDGM-NKQ7OFC2KRF4CIE', 'CLASH OF CLANS', '2924883', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GTYOM1CKSUZU1ZY', '2023-08-16 15:25:51'),
('IDGM-NKRGUFEZWYTUHNM', 'CLASH OF CLANS', '6636834', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('IDGM-NLUE9K22DEKRSNI', 'FREE FIRE', '7287881', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U1B8KFHBWZBPIRG', '2023-08-16 15:25:51'),
('IDGM-NMSPERFRFUHMUMP', 'GENSHIN IMPACT', '9616765', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-K3OWW3GSTTHTKFP', '2023-08-16 15:25:51'),
('IDGM-NNGEABQGWW0PETF', 'MOBILE LEGENDS', '8114471', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SDF4F91WYCLMWVN', '2023-08-16 15:25:51'),
('IDGM-NWPMNJDNBZKBEAG', 'PUBG MOBILE', '1876371', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZNJERALDVNLNIPG', '2023-08-16 15:25:51'),
('IDGM-NYHZRBQ9MHKUNEM', 'FREE FIRE', '9718239', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GKFVZMBQMHXQEPO', '2023-08-16 15:25:51'),
('IDGM-NZD2I57I5XKJJNK', 'CLASH OF CLANS', '5951287', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GDLUX5L9H6PT7M2', '2023-08-16 15:25:51'),
('IDGM-O9IYCLZU7OOOICQ', 'MOBILE LEGENDS', '6134862', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-R4T2QOEBZJFJLZD', '2023-08-16 15:25:51'),
('IDGM-OAQAQPESHOYGLAF', 'MOBILE LEGENDS', '9513913', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FGFBKLWMSS1M03R', '2023-08-16 15:25:51'),
('IDGM-OBEJX8NYVI4ZY7M', 'FREE FIRE', '5454258', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('IDGM-OGFY4HY6Q4JK3PE', 'MOBILE LEGENDS', '7294676', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('IDGM-OJ3I7JSV6CXLZLT', 'MOBILE LEGENDS', '9195319', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('IDGM-OJLGF4KJXQQI9RG', 'PUBG MOBILE', '4916247', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-OOWKSFGUERAZHEK', 'PUBG MOBILE', '9616391', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IF1LVNXVJI6MNJC', '2023-08-16 15:25:51'),
('IDGM-OPTCAXQSURGIQVA', 'PUBG MOBILE', '3357592', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-Q9DAMYWHNYBLQ2A', '2023-08-16 15:25:51'),
('IDGM-OQUUDAMFICTKGRS', 'CLASH OF CLANS', '6862774', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-K8QOAMBGQZPJFSN', '2023-08-16 15:25:51'),
('IDGM-OQZZJJUCC3NVOTS', 'PUBG MOBILE', '2147221', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-K3OWW3GSTTHTKFP', '2023-08-16 15:25:51'),
('IDGM-ORT5AEU0SUO6NXZ', 'PUBG MOBILE', '6965867', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-OXHSMOXYDHIBANW', 'FREE FIRE', '6947436', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UTCUVBAIO7QOCF7', '2023-08-16 15:25:51'),
('IDGM-P0NL3KNIQP6KVTG', 'MOBILE LEGENDS', '7251453', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-45APCIKVCYS2PTL', '2023-08-16 15:25:51'),
('IDGM-P5CTUVX0WJK13UZ', 'FREE FIRE', '8252889', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZKITBG6QVJPHCJC', '2023-08-16 15:25:51'),
('IDGM-P5U0CH9QFBZ5CYP', 'GENSHIN IMPACT', '9143247', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RBFWKQSIBRN998M', '2023-08-16 15:25:51'),
('IDGM-P94OPLVDBODP8OG', 'CLASH OF CLANS', '1845862', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U1B8KFHBWZBPIRG', '2023-08-16 15:25:51'),
('IDGM-PAKUYXPLAGGNI2Q', 'MOBILE LEGENDS', '6513166', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-MV1GIBQFT7BQP9N', '2023-08-16 15:25:51'),
('IDGM-PB6UI3JQZZXB8M0', 'FREE FIRE', '1498212', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HUEOW7NXUDXM8HE', '2023-08-16 15:25:51'),
('IDGM-PDV2AXICJIMJZBU', 'FREE FIRE', '2474948', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZIJ2P6JFGFSMQDR', '2023-08-16 15:25:51'),
('IDGM-PH3UTAKFHHRIEH3', 'FREE FIRE', '5959154', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-27QELP0JIRGFMVA', '2023-08-16 15:25:51'),
('IDGM-PIODBSK6TF6SSLO', 'FREE FIRE', '2511432', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-PK9O9LTDNJCLUF7', 'MOBILE LEGENDS', '7775312', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YENXXSVKJPJ0YAD', '2023-08-16 15:25:51'),
('IDGM-PLLOUOYLU5GRWYU', 'MOBILE LEGENDS', '1431618', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QZF22QPKLFOCAXY', '2023-08-16 15:25:51'),
('IDGM-PMT5L5WVPTYQXAL', 'FREE FIRE', '4329661', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('IDGM-PONROHRJQJA2KEL', 'PUBG MOBILE', '8398164', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('IDGM-PQAQSWWC5USXHTO', 'PUBG MOBILE', '4264235', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-3VOIUAZRQM5D7SQ', '2023-08-16 15:25:51'),
('IDGM-PS23LMG0D2EKUWU', 'PUBG MOBILE', '3644479', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('IDGM-Q3ZT93ZIUEM6NUF', 'CLASH OF CLANS', '5491955', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RBFWKQSIBRN998M', '2023-08-16 15:25:51'),
('IDGM-Q88FZDDJGUNL4IQ', 'CLASH OF CLANS', '8252657', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('IDGM-QBH59NNOG265YOW', 'PUBG MOBILE', '2394145', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('IDGM-QBPTSTC1JWNV9TT', 'FREE FIRE', '8739477', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('IDGM-QC8BDF2VZ5DSP8J', 'PUBG MOBILE', '2781339', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('IDGM-QCVGSXLXEYDWY3F', 'CLASH OF CLANS', '6342263', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W84IL6WOLXZTWHF', '2023-08-16 15:25:51'),
('IDGM-QCWUBVFARGMAGGC', 'GENSHIN IMPACT', '5588854', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-QFFRKMFNRAIKSV2', 'GENSHIN IMPACT', '9941132', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YENXXSVKJPJ0YAD', '2023-08-16 15:25:51'),
('IDGM-QFIQDOZHPYTYNU1', 'GENSHIN IMPACT', '2868547', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZNJERALDVNLNIPG', '2023-08-16 15:25:51'),
('IDGM-QGYJSYMEVA0EDDH', 'CLASH OF CLANS', '7112244', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-21XRJBRHXLU5H11', '2023-08-16 15:25:51'),
('IDGM-QHTDASPFQIBDN6H', 'PUBG MOBILE', '5636839', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-DURPMA7MHMQBMBG', '2023-08-16 15:25:51'),
('IDGM-QLHEOPCTNZJDLSU', 'CLASH OF CLANS', '1342127', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7PI8XLQZ0TKMONS', '2023-08-16 15:25:51'),
('IDGM-QLMLTQVG3WOMPW7', 'MOBILE LEGENDS', '7275962', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CBR0IYLZS7UINWG', '2023-08-16 15:25:51'),
('IDGM-QM9TQQCHFT54BRQ', 'PUBG MOBILE', '6139163', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-3GDRLSN7HOKVC2E', '2023-08-16 15:25:51'),
('IDGM-QMKIIWIPIDE5OQ6', 'FREE FIRE', '2387257', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BU3IRXEITHMP6U3', '2023-08-16 15:25:51'),
('IDGM-QN7LQALLOXHMYXS', 'FREE FIRE', '2421159', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-21XRJBRHXLU5H11', '2023-08-16 15:25:51'),
('IDGM-QNE8GIL6ZLIF4C6', 'MOBILE LEGENDS', '4225734', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-3VOIUAZRQM5D7SQ', '2023-08-16 15:25:51'),
('IDGM-QNYDRQOBW5TUA5F', 'FREE FIRE', '6451476', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AIBLPPSMTCRK06A', '2023-08-16 15:25:51'),
('IDGM-QPQ5VRRZ0FO9U3C', 'GENSHIN IMPACT', '4753173', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPADIWFZFNBMOWR', '2023-08-16 15:25:51'),
('IDGM-QXSV5ZHPTO2Y7UT', 'PUBG MOBILE', '3836927', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7PI8XLQZ0TKMONS', '2023-08-16 15:25:51'),
('IDGM-QZSBNJJCNY7T0ZW', 'GENSHIN IMPACT', '7761295', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZNJERALDVNLNIPG', '2023-08-16 15:25:51'),
('IDGM-R5MOH0C87WOSEL4', 'CLASH OF CLANS', '6535632', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZIJ2P6JFGFSMQDR', '2023-08-16 15:25:51'),
('IDGM-R7L1L8UH8CFXALB', 'MOBILE LEGENDS', '2692324', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('IDGM-R8BXXGKVIGNP3CP', 'FREE FIRE', '3726317', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-3GDRLSN7HOKVC2E', '2023-08-16 15:25:51'),
('IDGM-RCE809OV74QNJD1', 'MOBILE LEGENDS', '8668151', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IIOSZF73JJFQ6TW', '2023-08-16 15:25:51'),
('IDGM-RFGRRIWVS2QOPCD', 'CLASH OF CLANS', '6676955', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FRSXJFIMBRL212V', '2023-08-16 15:25:51'),
('IDGM-RKMYJFW97JRZSSV', 'PUBG MOBILE', '5324333', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FCIOSOV0Z7JTGAL', '2023-08-16 15:25:51'),
('IDGM-RSRLCCIQXDEQBHJ', 'CLASH OF CLANS', '2994417', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSENDCFVN07T7Y7', '2023-08-16 15:25:51'),
('IDGM-RUAWQZUTVLCN1QC', 'PUBG MOBILE', '1348764', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('IDGM-RXJJTYVO87ZH1L6', 'PUBG MOBILE', '7248171', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-3GDRLSN7HOKVC2E', '2023-08-16 15:25:51'),
('IDGM-RZ7UPZYTVYC9UOL', 'PUBG MOBILE', '6514719', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('IDGM-RZBUTMHC4L03N3B', 'MOBILE LEGENDS', '7631819', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AEVQG3FDV0Y3DM6', '2023-08-16 15:25:51'),
('IDGM-S6VRSLZU9XLCUDL', 'FREE FIRE', '7966215', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-GTYOM1CKSUZU1ZY', '2023-08-16 15:25:51'),
('IDGM-S7S9ENTC9UM89BG', 'MOBILE LEGENDS', '2814153', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-MV1GIBQFT7BQP9N', '2023-08-16 15:25:51'),
('IDGM-SAZUX7YSPPTUFPA', 'CLASH OF CLANS', '2167175', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EDM6TV3S11D9IAS', '2023-08-16 15:25:51'),
('IDGM-SCYHJWKX7RYPHAF', 'FREE FIRE', '1749994', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ONNTEZDTNGIYV6R', '2023-08-16 15:25:51'),
('IDGM-SDIX7GQQXAPNMZ0', 'CLASH OF CLANS', '5827581', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('IDGM-SS3EUJDPUEXROUU', 'FREE FIRE', '4233472', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('IDGM-SWTBBB8ZBJ8HBHT', 'MOBILE LEGENDS', '5411946', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('IDGM-SX4YKRGS6PBCWCY', 'GENSHIN IMPACT', '4569339', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-PR6SSGMRFR3IQWK', '2023-08-16 15:25:51'),
('IDGM-T6QBRWQZJ12BYLT', 'PUBG MOBILE', '7938535', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ML70PYUK8YMF7IQ', '2023-08-16 15:25:51'),
('IDGM-TD0HFMH5M3CCBYW', 'MOBILE LEGENDS', '6924583', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('IDGM-THIQ8ZO0JUIKFKP', 'FREE FIRE', '3553678', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('IDGM-TKA3M6WKCLHV4PB', 'PUBG MOBILE', '5622845', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CZZYKMUCFQRYR7N', '2023-08-16 15:25:51'),
('IDGM-TMQN6AURMFYUBZA', 'FREE FIRE', '7177434', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BAABXXSPS3MY5B1', '2023-08-16 15:25:51'),
('IDGM-TMTJ7BF57VKNTGC', 'MOBILE LEGENDS', '9317598', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('IDGM-TRWIISHCUIEZFYM', 'GENSHIN IMPACT', '8727324', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSENDCFVN07T7Y7', '2023-08-16 15:25:51'),
('IDGM-TW1JYVWVQBZAAOE', 'CLASH OF CLANS', '2959659', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SM1DPTXBO0FLQEA', '2023-08-16 15:25:51'),
('IDGM-TYGFUJV892G1FMK', 'MOBILE LEGENDS', '3857292', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NMEKYNSTLTFW4JK', '2023-08-16 15:25:51'),
('IDGM-U2MD7E9N5MOVHGB', 'MOBILE LEGENDS', '4529644', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YENXXSVKJPJ0YAD', '2023-08-16 15:25:51');
INSERT INTO `ID_GAMES` (`ID_IDGAME`, `JENIS_GAME`, `ID_GAME`, `RANKED`, `SCREENSHOOT_1`, `SCREENSHOOT_2`, `SCREENSHOOT_3`, `ID_USER`, `CREATED_AT`) VALUES
('IDGM-U5OKQJ2I0CBERXM', 'MOBILE LEGENDS', '7215172', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-OJ1ODV7DLF6USHE', '2023-08-16 15:25:51'),
('IDGM-U6STXLDRUP6IDK7', 'GENSHIN IMPACT', '3132618', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ALR6WNLRSNEKZAB', '2023-08-16 15:25:51'),
('IDGM-U84HZTB9TH63HDC', 'PUBG MOBILE', '2948973', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('IDGM-UAD6UHJXQYWVIHE', 'CLASH OF CLANS', '1346921', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AEVQG3FDV0Y3DM6', '2023-08-16 15:25:51'),
('IDGM-UFOWEDE0AYOJKRJ', 'GENSHIN IMPACT', '7172286', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-8YFANWOOPQ60QET', '2023-08-16 15:25:51'),
('IDGM-UHSFO3DIJDNZXWV', 'PUBG MOBILE', '8435478', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-C89TCPVZUDQZ6Y3', '2023-08-16 15:25:51'),
('IDGM-UKELX2QBIT7SOSZ', 'FREE FIRE', '9663859', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('IDGM-UKEVISUJDK5VJEE', 'CLASH OF CLANS', '7783552', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HDDY9VYYSAVSK1B', '2023-08-16 15:25:51'),
('IDGM-ULHZ3VOBELU4UUH', 'GENSHIN IMPACT', '4459353', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FRSXJFIMBRL212V', '2023-08-16 15:25:51'),
('IDGM-ULMNVWXCFZBWEKP', 'GENSHIN IMPACT', '8577878', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QZF22QPKLFOCAXY', '2023-08-16 15:25:51'),
('IDGM-UNEJMJ2D9QL0TCW', 'CLASH OF CLANS', '7567445', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RWQKQKONKLQARWJ', '2023-08-16 15:25:51'),
('IDGM-URGZR5X2YUHBPAL', 'PUBG MOBILE', '4414472', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('IDGM-UY0WMAXAVGZYCNB', 'MOBILE LEGENDS', '5757898', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('IDGM-UY4LRYMAS7VAGEX', 'PUBG MOBILE', '6512283', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-VKUS2BUUPXNIEMZ', '2023-08-16 15:25:51'),
('IDGM-UYTMIGFINHEFSNQ', 'FREE FIRE', '6987585', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HUEOW7NXUDXM8HE', '2023-08-16 15:25:51'),
('IDGM-UZRHQYXE7SXLD6R', 'CLASH OF CLANS', '1738189', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('IDGM-UZWH6IC6DPZVK1W', 'GENSHIN IMPACT', '4865712', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('IDGM-V5CJENY1KXINRN7', 'CLASH OF CLANS', '3792419', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZY1ZNAZPQPBKRQ3', '2023-08-16 15:25:51'),
('IDGM-V5K0KDJBPMNZLNL', 'CLASH OF CLANS', '9887698', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('IDGM-VJ8YHIS7PYVCPWZ', 'GENSHIN IMPACT', '2752147', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-YEECAXHKXLUK7M4', '2023-08-16 15:25:51'),
('IDGM-VKHW2PCU8R8D5E5', 'PUBG MOBILE', '3775274', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-IDEG6LS7E5CJTHA', '2023-08-16 15:25:51'),
('IDGM-VKM2BCPOQBZTBWY', 'CLASH OF CLANS', '6968152', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FRSXJFIMBRL212V', '2023-08-16 15:25:51'),
('IDGM-VMOZCDZLY9V04YE', 'PUBG MOBILE', '6763517', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AIBLPPSMTCRK06A', '2023-08-16 15:25:51'),
('IDGM-VROGN2AAF0M1OOF', 'FREE FIRE', '7492346', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('IDGM-VULVE87PBIDOIZ1', 'CLASH OF CLANS', '4789824', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LBKSFXILWRIFW60', '2023-08-16 15:25:51'),
('IDGM-VV7W3ZUSS63JXNV', 'CLASH OF CLANS', '7224231', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CM1O0VLGKLBCW92', '2023-08-16 15:25:51'),
('IDGM-VZY36IBLPY1OAJF', 'CLASH OF CLANS', '4371768', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-Q9DAMYWHNYBLQ2A', '2023-08-16 15:25:51'),
('IDGM-W82UKNVVCGJPDMU', 'GENSHIN IMPACT', '3163845', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('IDGM-WDGFCLENAAMTLVN', 'MOBILE LEGENDS', '3245728', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZKITBG6QVJPHCJC', '2023-08-16 15:25:51'),
('IDGM-WHRHQ9OQLWNGUFD', 'GENSHIN IMPACT', '9584723', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('IDGM-WJFQJQZOROJJ6AQ', 'FREE FIRE', '3794234', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-54EATO6DBBJCGRG', '2023-08-16 15:25:51'),
('IDGM-WPT3KJY4MBWOLA2', 'FREE FIRE', '3649968', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('IDGM-WRUWOCIDPCJ6IWL', 'MOBILE LEGENDS', '3148188', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('IDGM-WU3Y1GGFICF1VCO', 'FREE FIRE', '3213287', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-BAABXXSPS3MY5B1', '2023-08-16 15:25:51'),
('IDGM-WUGENTAYRVB2VW7', 'FREE FIRE', '2974366', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SM1DPTXBO0FLQEA', '2023-08-16 15:25:51'),
('IDGM-XABL5BQDJRIBE7K', 'FREE FIRE', '1755192', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ONNTEZDTNGIYV6R', '2023-08-16 15:25:51'),
('IDGM-XBQBLSQNYC7JTET', 'GENSHIN IMPACT', '8583929', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XPIZMO5FK8CFRT3', '2023-08-16 15:25:51'),
('IDGM-XGDCMIQSPHRON1N', 'CLASH OF CLANS', '7133818', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-FEZ7IRLU2CWUL75', '2023-08-16 15:25:51'),
('IDGM-XI1ZGKLMCPD89KE', 'FREE FIRE', '8861375', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-JYGOHJHVUWKWGBL', '2023-08-16 15:25:51'),
('IDGM-XILLJWXXHZT8Q5K', 'CLASH OF CLANS', '9654399', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-66QKYWU46VS2CCC', '2023-08-16 15:25:51'),
('IDGM-XLCIDYG3PE9IZH9', 'CLASH OF CLANS', '2479314', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-1TXMVVUZU7E9B8A', '2023-08-16 15:25:51'),
('IDGM-XNLOZH7MF3MC8MJ', 'CLASH OF CLANS', '9628929', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('IDGM-XQOM7CL00P6JF1O', 'MOBILE LEGENDS', '5326144', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LOLWQQK4INFH3QT', '2023-08-16 15:25:51'),
('IDGM-Y54QUIUS7ZXLEDI', 'MOBILE LEGENDS', '7225299', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UEQ1E3TDPMJNBSH', '2023-08-16 15:25:51'),
('IDGM-YJWUFLTJJRWG9LV', 'CLASH OF CLANS', '2962136', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EWXZDXF8WJLXKCL', '2023-08-16 15:25:51'),
('IDGM-YKSZAWEPEIATUIZ', 'CLASH OF CLANS', '6844161', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-HDDY9VYYSAVSK1B', '2023-08-16 15:25:51'),
('IDGM-YPDRZUC3EJGQVZK', 'MOBILE LEGENDS', '5241671', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('IDGM-YPUCZ2V0MCAPXDV', 'FREE FIRE', '1268394', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KU23AWTK1KT9IB4', '2023-08-16 15:25:51'),
('IDGM-YXHQL08UH44NHPQ', 'FREE FIRE', '5661665', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('IDGM-Z0BFVQVGGICAYIZ', 'FREE FIRE', '7199169', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-TELYXBMV55K5RBJ', '2023-08-16 15:25:51'),
('IDGM-Z1YZQPAKEETB31Y', 'CLASH OF CLANS', '8969137', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('IDGM-Z6Z0WJSLEAHFSUS', 'MOBILE LEGENDS', '1991958', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('IDGM-ZGE4JKX1SOFH3YQ', 'GENSHIN IMPACT', '6142641', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-S9ZJGKCUHBQPDBP', '2023-08-16 15:25:51'),
('IDGM-ZIOKFLFMTMLZKAJ', 'GENSHIN IMPACT', '2826738', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('IDGM-ZKODLLYX75RCOVT', 'PUBG MOBILE', '7926177', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-ZEB9WQWXA7M81CK', '2023-08-16 15:25:51'),
('IDGM-ZR31O4GMHQMGSDJ', 'CLASH OF CLANS', '1264377', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-LBKSFXILWRIFW60', '2023-08-16 15:25:51'),
('IDGM-ZT5MSCKF0SOWIK8', 'PUBG MOBILE', '5516465', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-EDM6TV3S11D9IAS', '2023-08-16 15:25:51'),
('IDGM-ZUYA1O5L6JTA7UA', 'FREE FIRE', '1778475', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-XPIZMO5FK8CFRT3', '2023-08-16 15:25:51'),
('IDGM-ZWNFOT1AOHTE7XN', 'PUBG MOBILE', '7125822', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-UTCUVBAIO7QOCF7', '2023-08-16 15:25:51'),
('IDGM-ZYLIKXU7QYG1JD4', 'FREE FIRE', '9752634', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-AEVQG3FDV0Y3DM6', '2023-08-16 15:25:51'),
('IDGM-ZZNHQBCXMQMHROB', 'GENSHIN IMPACT', '8721237', 'test', '16920426571692042657_e786e431097a1ca79210.png', NULL, NULL, 'USRS-45APCIKVCYS2PTL', '2023-08-16 15:25:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `LOCATIONS`
--

CREATE TABLE `LOCATIONS` (
  `ID_LOCATION` char(20) NOT NULL,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL,
  `DESA` varchar(50) NOT NULL,
  `KECAMATAN` varchar(50) NOT NULL,
  `KABUPATEN` varchar(50) NOT NULL,
  `PROVINSI` varchar(50) NOT NULL,
  `ID_USER` char(20) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `LOCATIONS`
--

INSERT INTO `LOCATIONS` (`ID_LOCATION`, `LATITUDE`, `LONGITUDE`, `DESA`, `KECAMATAN`, `KABUPATEN`, `PROVINSI`, `ID_USER`, `CREATED_AT`) VALUES
('L-ABVVILCH0095313508', '-7.86896372949255', '113.32842252131693', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-HPYGJ5HR5UJL1ZP', '2023-08-16 15:25:51'),
('L-AFPIAZHV0011404021', '-7.87736395466344', '113.34396925942327', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ZEB9WQWXA7M81CK', '2023-08-16 15:25:51'),
('L-AMQTRCIN0079208306', '-7.88625557889526', '113.39712716798242', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-KU23AWTK1KT9IB4', '2023-08-16 15:25:51'),
('L-AOWXGIYU0053487849', '-7.83726472743816', '113.37877531552913', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-UVQ78B7A6A7MD2E', '2023-08-16 15:25:51'),
('L-ASFPTZDH0084081381', '-7.84693638986311', '113.39995345971682', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-SIBGYKYHXYOUMKI', '2023-08-16 15:25:51'),
('L-BGVCSEUZ0021277781', '-7.81486879738262', '113.36143188359455', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-BAABXXSPS3MY5B1', '2023-08-16 15:25:51'),
('L-BLYANMGH0099556471', '-7.84344458575448', '113.34294379359517', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-K3OWW3GSTTHTKFP', '2023-08-16 15:25:51'),
('L-BNYGFGMO0038649527', '-7.85399236982315', '113.31464918325191', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-W0BEVCHJTZUYUVE', '2023-08-16 15:25:51'),
('L-BTDDBKTS0029167601', '-7.83196847974153', '113.35414746384645', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-VKUS2BUUPXNIEMZ', '2023-08-16 15:25:51'),
('L-CSCTSAKR0024905820', '-7.84517357586684', '113.36744698724139', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-U1B8KFHBWZBPIRG', '2023-08-16 15:25:51'),
('L-CUQBFGZV0041929999', '-7.87357166494362', '113.36259264675684', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-RSENDCFVN07T7Y7', '2023-08-16 15:25:51'),
('L-DAOQWITW0087733261', '-7.88263631771595', '113.34226845484895', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-S9ZJGKCUHBQPDBP', '2023-08-16 15:25:51'),
('L-DAWQCOZR0044773136', '-7.81125361293887', '113.38786695913894', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-GDLUX5L9H6PT7M2', '2023-08-16 15:25:51'),
('L-DBIQMFEL0002269304', '-7.86568818658925', '113.35659491382584', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-UTCUVBAIO7QOCF7', '2023-08-16 15:25:51'),
('L-DEIUVIIS0066213915', '-7.85118982236739', '113.33914185521662', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-PR6SSGMRFR3IQWK', '2023-08-16 15:25:51'),
('L-DIBCWBAD0019338434', '-7.83537649382693', '113.38986612667583', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-B5HY9ETVPIF2X8F', '2023-08-16 15:25:51'),
('L-EBMEPBUO0094423081', '-7.85184541238443', '113.35811338768617', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-QLOSQNVHEIULWPV', '2023-08-16 15:25:51'),
('L-EHDQFQUZ0007690783', '-7.85922312877361', '113.35642615261521', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-66QKYWU46VS2CCC', '2023-08-16 15:25:51'),
('L-EKUYNOXG0025546906', '-7.88342647558695', '113.31219544398129', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-YEECAXHKXLUK7M4', '2023-08-16 15:25:51'),
('L-EYAJUIRD0061835612', '-7.83924998714352', '113.31785382372167', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-SNIOJTVRBHDNVZQ', '2023-08-16 15:25:51'),
('L-EYJGVMIV0036488293', '-7.88934786863733', '113.31567817561746', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-CZZYKMUCFQRYR7N', '2023-08-16 15:25:51'),
('L-FDGECUOJ0094070218', '-7.88562922863791', '113.32251769129238', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-I9QWX1H4XO39VKO', '2023-08-16 15:25:51'),
('L-FOXYBVHQ0027602261', '-7.83586554845894', '113.35173983913291', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ROMQMFQPDYX1XBI', '2023-08-16 15:25:51'),
('L-FQYEUEEU0052972535', '-7.85134761737956', '113.39155366716555', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ALR6WNLRSNEKZAB', '2023-08-16 15:25:51'),
('L-FRNBULLQ0059064466', '-7.85236789629745', '113.38836658766382', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-YENXXSVKJPJ0YAD', '2023-08-16 15:25:51'),
('L-FUDVQKTE0016949076', '-7.83531464449294', '113.34576726319544', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-54EATO6DBBJCGRG', '2023-08-16 15:25:51'),
('L-FWPVJIJM0033549351', '-7.81832129651998', '113.33243129773278', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ZNJERALDVNLNIPG', '2023-08-16 15:25:51'),
('L-GFTLNGXP0056674687', '-7.86985748521639', '113.31691675678629', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-DLC4AQSJ0MSMLIG', '2023-08-16 15:25:51'),
('L-GGGLOTYV0079066466', '-7.87199851368757', '113.36467812731919', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-7YEIRHYSN2M1WIC', '2023-08-16 15:25:51'),
('L-HBZFHEHL0017276629', '-7.87795395155548', '113.31933977468326', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ZY1ZNAZPQPBKRQ3', '2023-08-16 15:25:51'),
('L-HEGASCYY0020155276', '-7.83743932771158', '113.33594579376288', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-AEVQG3FDV0Y3DM6', '2023-08-16 15:25:51'),
('L-HHXFWYWH0063788715', '-7.86881773356913', '113.32459834867865', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-JPADIWFZFNBMOWR', '2023-08-16 15:25:51'),
('L-HNGISXHJ0037071099', '-7.82686747594188', '113.32199857239856', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-EWXZDXF8WJLXKCL', '2023-08-16 15:25:51'),
('L-HXYKCMWG0010258998', '-7.87827493476355', '113.37139692456134', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-WF91IBUZIQXYJDH', '2023-08-16 15:25:51'),
('L-HZWZPURC0031548162', '-7.82591974917285', '113.36393952353644', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-45APCIKVCYS2PTL', '2023-08-16 15:25:51'),
('L-INEMPTDK0082310045', '-7.82668778787481', '113.36919455813163', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-SM1DPTXBO0FLQEA', '2023-08-16 15:25:51'),
('L-IQJNEZAS0012168120', '-7.86525948745672', '113.38473373414849', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-KWE6S8WFY2MVSGZ', '2023-08-16 15:25:51'),
('L-JNPKMLUE0059215577', '-7.81937162667375', '113.34299799564764', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-CKOA3Q9T0L3QFYF', '2023-08-16 15:25:51'),
('L-JRPXJBLW0065305071', '-7.81789877977569', '113.33212188964746', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ONNTEZDTNGIYV6R', '2023-08-16 15:25:51'),
('L-JXFHQKAR0093810937', '-7.89882975337326', '113.35358232992142', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-GKFVZMBQMHXQEPO', '2023-08-16 15:25:51'),
('L-KFGOGNXH0049160381', '-7.88187384919853', '113.35973552356325', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-IDEG6LS7E5CJTHA', '2023-08-16 15:25:51'),
('L-KFMOWMJI0046165730', '-7.85627834739537', '113.31469276927413', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-FRSXJFIMBRL212V', '2023-08-16 15:25:51'),
('L-LCILOROB0059876605', '-7.809969585285535', '113.37248504161833', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-T5BIWEOWTB1WEJP', '2023-08-24 14:55:09'),
('L-LISZQYJP0085026578', '-7.83553333755473', '113.33913413385155', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-U2A0DPTSUPVN25K', '2023-08-16 15:25:51'),
('L-LLOCACSF0060905619', '-7.83749475821293', '113.39446784352322', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-IF1LVNXVJI6MNJC', '2023-08-16 15:25:51'),
('L-LQXWPTOJ0098120341', '-7.81215958627293', '113.37741926485675', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-JPTHECTSO7EVXCC', '2023-08-16 15:25:51'),
('L-LREOKXWX0050944750', '-7.82192181474625', '113.32928227916326', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-RBFWKQSIBRN998M', '2023-08-16 15:25:51'),
('L-LSXRSAZV0082473898', '-7.87753237761262', '113.35911978444341', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-JYGOHJHVUWKWGBL', '2023-08-16 15:25:51'),
('L-MGOYABCK0027164356', '-7.84231633712169', '113.34925411798282', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-27QELP0JIRGFMVA', '2023-08-16 15:25:51'),
('L-MIEPVKWT0022713578', '-7.85872698551393', '113.31537126525773', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-MV1GIBQFT7BQP9N', '2023-08-16 15:25:51'),
('L-MSQOAQAI0015419446', '-7.82487981135311', '113.38359363664672', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-HUEOW7NXUDXM8HE', '2023-08-16 15:25:51'),
('L-MUYOAXEM0050304191', '-7.81567151239554', '113.31166893214315', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-1TXMVVUZU7E9B8A', '2023-08-16 15:25:51'),
('L-MVWAAAJB0028878731', '-7.86764516663635', '113.38129593926268', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-UEQ1E3TDPMJNBSH', '2023-08-16 15:25:51'),
('L-NFJCEVBT0002510647', '-7.85536939343878', '113.37854663412143', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-XS3ED25FKYTFBR5', '2023-08-16 15:25:51'),
('L-NKRUGFWE0045565016', '-7.83394799259866', '113.31649946851995', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-W84IL6WOLXZTWHF', '2023-08-16 15:25:51'),
('L-NUHFUCQV0085759279', '-7.84119356567237', '113.33726164413554', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-3VOIUAZRQM5D7SQ', '2023-08-16 15:25:51'),
('L-NZFGEIRQ0031388673', '-7.84363144736331', '113.32677669299215', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-CBR0IYLZS7UINWG', '2023-08-16 15:25:51'),
('L-OPXFDVFY0034351326', '-7.82936781339141', '113.36777955268166', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-GBGBYZRKMHBLJSO', '2023-08-16 15:25:51'),
('L-PEVENGOC0037860840', '-7.87945536999641', '113.31633491634827', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-UFSTAUVGXDO7HYN', '2023-08-16 15:25:51'),
('L-PGQSPNUL0041997097', '-7.88446658293852', '113.39413778735986', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-RWQKQKONKLQARWJ', '2023-08-16 15:25:51'),
('L-PIAVBNAW0079754093', '-7.85149843969399', '113.31634747973355', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-RSIGHJE97AWKY05', '2023-08-16 15:25:51'),
('L-PIJVSVBP0061080779', '-7.88389479923776', '113.36173237395468', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-3GDRLSN7HOKVC2E', '2023-08-16 15:25:51'),
('L-PJETFEET0009827576', '-7.83273961924841', '113.37793198646452', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-FEZ7IRLU2CWUL75', '2023-08-16 15:25:51'),
('L-PUVYIDEU0095576328', '-7.88361958554915', '113.32128772164669', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ZIJ2P6JFGFSMQDR', '2023-08-16 15:25:51'),
('L-QPPKDIHG0099488048', '-7.84194437581465', '113.36118763381357', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-C89TCPVZUDQZ6Y3', '2023-08-16 15:25:51'),
('L-QUMSHXHH0077680261', '-7.83195551382476', '113.39213781154741', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-W2EGPWFMMRRDX6I', '2023-08-16 15:25:51'),
('L-RGJVYUKK0067523158', '-7.82217227739783', '113.38792932478456', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-OJ1ODV7DLF6USHE', '2023-08-16 15:25:51'),
('L-RMHCSTXI0059936604', '-7.83976831482752', '113.36592644487533', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-HDDY9VYYSAVSK1B', '2023-08-16 15:25:51'),
('L-RQNBLJAI0067234307', '-7.88185787565477', '113.37593822888894', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-7PI8XLQZ0TKMONS', '2023-08-16 15:25:51'),
('L-SCIEXKWW0007197432', '-7.87426685996863', '113.37378153717131', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-NZOKMYJLURN37CV', '2023-08-16 15:25:51'),
('L-SEJFUITW0089688326', '-7.89372749128158', '113.33555366544572', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-FCIOSOV0Z7JTGAL', '2023-08-16 15:25:51'),
('L-SFCISRHF0000363877', '-7.82433693478373', '113.34457688468876', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-K8QOAMBGQZPJFSN', '2023-08-16 15:25:51'),
('L-SPQMVGZH0041295568', '-7.82327468454932', '113.32111837788183', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-LBKSFXILWRIFW60', '2023-08-16 15:25:51'),
('L-SRPTBBAK0054679029', '-7.85689241426915', '113.37179224481411', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-SDF4F91WYCLMWVN', '2023-08-16 15:25:51'),
('L-TPFLQSLP0012617585', '-7.89517414814838', '113.38356186339341', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-CM1O0VLGKLBCW92', '2023-08-16 15:25:51'),
('L-TPWEKJFK0097336160', '-7.89682176266893', '113.39589628766565', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-R4T2QOEBZJFJLZD', '2023-08-16 15:25:51'),
('L-TSSDQZRN0010620616', '-7.88586349739219', '113.38864237173434', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-X7P96132YBENCLY', '2023-08-16 15:25:51'),
('L-TUMKDSUL0092451531', '-7.87542147414789', '113.38723817734361', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-8YFANWOOPQ60QET', '2023-08-16 15:25:51'),
('L-TZJNPQYM0058662011', '-7.85684845644524', '113.37929313647196', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-NMEKYNSTLTFW4JK', '2023-08-16 15:25:51'),
('L-UBTLIXKN0061150641', '-7.82556544543369', '113.31393841585315', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-Q9DAMYWHNYBLQ2A', '2023-08-16 15:25:51'),
('L-UCUCHJRC0036371006', '-7.87664635416628', '113.36694382435572', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-CQJ4VNAOFQT5WCK', '2023-08-16 15:25:51'),
('L-UCZXTAZV0014566239', '-7.81817648886261', '113.38743733334848', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-FGFBKLWMSS1M03R', '2023-08-16 15:25:51'),
('L-UDNSDJFY0036538059', '-7.82886981422664', '113.38662121273792', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-DURPMA7MHMQBMBG', '2023-08-16 15:25:51'),
('L-UXSABGKL0031909029', '-7.85826139374371', '113.32872168863852', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-TELYXBMV55K5RBJ', '2023-08-16 15:25:51'),
('L-UZAMAYCO0060206345', '-7.89612448112111', '113.36231782996485', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ZKITBG6QVJPHCJC', '2023-08-16 15:25:51'),
('L-VKHKZHKF0002705893', '-7.82263124231833', '113.32969388738514', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-LOLWQQK4INFH3QT', '2023-08-16 15:25:51'),
('L-WDYRHIBP0047033817', '-7.89743834147888', '113.37651465484884', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-ML70PYUK8YMF7IQ', '2023-08-16 15:25:51'),
('L-WHMMAVXK0093898860', '-7.86853181563526', '113.32956814344427', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-BU3IRXEITHMP6U3', '2023-08-16 15:25:51'),
('L-WYKGPTPB0022187828', '-7.81619515644693', '113.35969165235514', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-YP58RJ3I6WLG3Q8', '2023-08-16 15:25:51'),
('L-XBCQCQRC0091187651', '-7.83197781557398', '113.33553636337591', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-EDM6TV3S11D9IAS', '2023-08-16 15:25:51'),
('L-XDMFPRPO0085180997', '-7.85597888481434', '113.36174538251827', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-TBCWZ2ADEMK2U8M', '2023-08-16 15:25:51'),
('L-XINVWFDF0017249419', '-7.82558696495797', '113.38661719837169', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-4EY58QCE98HM3NL', '2023-08-16 15:25:51'),
('L-XKJLALIQ0064022728', '-7.85247483968677', '113.34356472789655', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-IIOSZF73JJFQ6TW', '2023-08-16 15:25:51'),
('L-XMSVAIDD0031317434', '-7.82588137711611', '113.32833597965531', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-XPIZMO5FK8CFRT3', '2023-08-16 15:25:51'),
('L-YOJTAARO0047229532', '-7.84963786685938', '113.35736115949227', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-GTYOM1CKSUZU1ZY', '2023-08-16 15:25:51'),
('L-YWZIZNRV0020170859', '-7.83287224753292', '113.31993362573831', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-QOAKPAYGWJKRBVL', '2023-08-16 15:25:51'),
('L-YXKKKRCS0000447526', '-7.88978357714988', '113.33268434925163', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-XSB8Q9RTSOLQUGW', '2023-08-16 15:25:51'),
('L-YYXGDEFP0098776384', '-7.81451743684312', '113.32211664968652', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-R8ZHQJWOUMFAFCD', '2023-08-16 15:25:51'),
('L-ZDQUWCVM0046470370', '-7.85439761768419', '113.36485219167978', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-AIBLPPSMTCRK06A', '2023-08-16 15:25:51'),
('L-ZJNZIBOR0007843230', '-7.83875347638478', '113.34889224717331', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-QZF22QPKLFOCAXY', '2023-08-16 15:25:51'),
('L-ZWFRRAZI0078562942', '-7.89915712691484', '113.32242911153866', 'Selogudig Kulon', 'Pajarakan', 'Probolinggo', 'Jawa Timur', 'USRS-21XRJBRHXLU5H11', '2023-08-16 15:25:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `PROFILES`
--

CREATE TABLE `PROFILES` (
  `ID_PROFILE` char(20) NOT NULL,
  `NAMA_LENGKAP` varchar(150) DEFAULT NULL,
  `TELEPON` char(13) NOT NULL,
  `JENIS_KELAMIN` enum('LAKI LAKI','PEREMPUAN') DEFAULT NULL,
  `STATUS` varchar(200) DEFAULT NULL,
  `FOTO` varchar(85) NOT NULL,
  `USIA` tinyint(4) NOT NULL,
  `LAST_ONLINE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `PROFILES`
--

INSERT INTO `PROFILES` (`ID_PROFILE`, `NAMA_LENGKAP`, `TELEPON`, `JENIS_KELAMIN`, `STATUS`, `FOTO`, `USIA`, `LAST_ONLINE`) VALUES
('USRS-1TXMVVUZU7E9B8A', 'MVLTXKORWBBLUNNTKMFF', '089368486529', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 13, NULL),
('USRS-21XRJBRHXLU5H11', 'VYAHJHFTBLJRUGMIDMXD', '089913283968', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 5, NULL),
('USRS-27QELP0JIRGFMVA', 'HWWXZSSSFYDWCGIAHGBY', '089003866058', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 40, NULL),
('USRS-3GDRLSN7HOKVC2E', 'ZEPJNGRIBINABHJOXHCW', '089688597361', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 7, NULL),
('USRS-3VOIUAZRQM5D7SQ', 'CDFXDPIKVCUUHGOUNYLH', '089471308838', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 21, NULL),
('USRS-45APCIKVCYS2PTL', 'UZHVMIYHMHWUDVTKOSFJ', '089541723352', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 56, NULL),
('USRS-4EY58QCE98HM3NL', 'QICSNUIXHZIQCJETMRMC', '089506379558', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 44, NULL),
('USRS-54EATO6DBBJCGRG', 'XRQIBWBGJAEIQJCQNUON', '089419465337', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 8, NULL),
('USRS-66QKYWU46VS2CCC', 'NEPQTOSCKCJPGYLWBERL', '089838250182', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 36, NULL),
('USRS-7PI8XLQZ0TKMONS', 'WXEQYMFELOFJJUTTYOQC', '089172045516', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 10, NULL),
('USRS-7YEIRHYSN2M1WIC', 'AOYVGLHJLVSIUTOWLVAO', '089452909357', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 46, NULL),
('USRS-8YFANWOOPQ60QET', 'UAIDWPTQOUGWAUHVPXFX', '089181067705', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 43, NULL),
('USRS-AEVQG3FDV0Y3DM6', 'MWJEBYAJMAGGBOAJIIYB', '089236774266', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 14, NULL),
('USRS-AIBLPPSMTCRK06A', 'MAKENHWOMZLDILFNBKBV', '089336669360', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 39, NULL),
('USRS-ALR6WNLRSNEKZAB', 'TISUBCLTZWFQNWZHJOMP', '089192376332', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 49, NULL),
('USRS-B5HY9ETVPIF2X8F', 'WOYKXBQAMPDKYXOLOLSP', '089708364015', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 13, NULL),
('USRS-BAABXXSPS3MY5B1', 'NYVYSTBRSGDXAXCFZCRU', '089913353499', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 44, NULL),
('USRS-BU3IRXEITHMP6U3', 'PDBRKQRBLFJMUDZBYFZZ', '089131143867', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 52, NULL),
('USRS-C89TCPVZUDQZ6Y3', 'GSGDSFRCRFSXCAFCWUZL', '089507691032', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 60, NULL),
('USRS-CBR0IYLZS7UINWG', 'CLLDKPUNCCOYUYMPDKAQ', '089222683160', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 13, NULL),
('USRS-CKOA3Q9T0L3QFYF', 'KINYQRHOHXBBRMNYKAFX', '089482394457', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 13, NULL),
('USRS-CM1O0VLGKLBCW92', 'VAHLMDNTTBFCHKSTOTHE', '089134538103', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 32, NULL),
('USRS-CQJ4VNAOFQT5WCK', 'FXXETVDWSKAMTKQWPBKK', '089445740984', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 36, NULL),
('USRS-CZZYKMUCFQRYR7N', 'SSQRGBQVMQIANZKGPGUC', '089810931142', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 20, NULL),
('USRS-DLC4AQSJ0MSMLIG', 'FSPVWNNCRVJWTUOLORFZ', '089711389009', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 30, NULL),
('USRS-DURPMA7MHMQBMBG', 'GXVMKRIVFOIXGVBABDKJ', '089101786167', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 15, NULL),
('USRS-EDM6TV3S11D9IAS', 'TQHOSBFLNXZOLCZZXFTK', '089373956702', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 43, NULL),
('USRS-EWXZDXF8WJLXKCL', 'LUTXVSLBHUZCEAHZRCTS', '089951191588', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 5, NULL),
('USRS-FCIOSOV0Z7JTGAL', 'FLIVEJBFGRGZEDLZOJTF', '089570318870', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 5, NULL),
('USRS-FEZ7IRLU2CWUL75', 'TGFEPSKPVUJQOAWERJGD', '089859026840', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 41, NULL),
('USRS-FGFBKLWMSS1M03R', 'KVLOFHDJNPDOZIFWVJHS', '089609390974', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 10, NULL),
('USRS-FRSXJFIMBRL212V', 'QLKVUBMYHEESDXSTBAIO', '089100370402', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 11, NULL),
('USRS-GBGBYZRKMHBLJSO', 'PARDDWVPDHKDTTUDRECL', '089725639082', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 35, NULL),
('USRS-GDLUX5L9H6PT7M2', 'LCDGGUANFXJZZXLRCRVV', '089462847602', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 10, NULL),
('USRS-GKFVZMBQMHXQEPO', 'HWAXFBXYJOINPLJJKSOL', '089979152965', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-GTYOM1CKSUZU1ZY', 'PWHSUGPCLHLVJRVNYAXW', '089366039810', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 23, NULL),
('USRS-HDDY9VYYSAVSK1B', 'ZWKYVGPEBQNYMDPTVILF', '089928213308', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 52, NULL),
('USRS-HPYGJ5HR5UJL1ZP', 'LPONVCEFKYGONVUEVKHL', '089432700371', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 35, NULL),
('USRS-HUEOW7NXUDXM8HE', 'ODRJWSGCQDTPINTXKRZU', '089198076612', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 24, NULL),
('USRS-I9QWX1H4XO39VKO', 'GJOEQSNRFBSPIHLTNZET', '089876113401', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 29, NULL),
('USRS-IDEG6LS7E5CJTHA', 'ISVLHFUQCSMGPXUWOOBW', '089762436785', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-IF1LVNXVJI6MNJC', 'KQSVUHFYTUSDSSXZMCER', '089696001595', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 7, NULL),
('USRS-IIOSZF73JJFQ6TW', 'GVASKALZSJZIUQUXCIIN', '089963257978', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 60, NULL),
('USRS-JPADIWFZFNBMOWR', 'DHNRDDCNWIUUTZWXNPSY', '089021088273', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 60, NULL),
('USRS-JPTHECTSO7EVXCC', 'NYFUFRKYZONEVRABUXAG', '089372379651', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 48, NULL),
('USRS-JYGOHJHVUWKWGBL', 'DQBXOVBHMHQROYGQRXOD', '089851075189', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 55, NULL),
('USRS-K3OWW3GSTTHTKFP', 'YWLXTOXXYKXHMSFAKMHC', '089193551258', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 38, NULL),
('USRS-K8QOAMBGQZPJFSN', 'IFSZHYRNUZQDUQEJQHWU', '089347478487', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 59, NULL),
('USRS-KU23AWTK1KT9IB4', 'NKAALLLSDSUZGDONAFNQ', '089301422319', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 37, NULL),
('USRS-KWE6S8WFY2MVSGZ', 'SGERSQHPGNBYQCOVWEUW', '089534680992', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 23, NULL),
('USRS-LBKSFXILWRIFW60', 'NVSMJEFMYIHOGIAIAMBU', '089538100392', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-LOLWQQK4INFH3QT', 'RZIRJNRMHQMRHBIKSNQA', '089480939416', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 29, NULL),
('USRS-ML70PYUK8YMF7IQ', 'UZJNOXWWBEZDFEGFFDLM', '089070752586', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 21, NULL),
('USRS-MV1GIBQFT7BQP9N', 'XMZIAXJDDGKWQOJACXPP', '089517049734', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 31, NULL),
('USRS-NMEKYNSTLTFW4JK', 'NSTHVFLVGSGSFQHHOZVS', '089719373520', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 42, NULL),
('USRS-NZOKMYJLURN37CV', 'UGRBDJOGCZQJTGAJRWDM', '089880100704', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-OJ1ODV7DLF6USHE', 'TAUSBNDVGDPUJUUXTCIJ', '089096117317', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 30, NULL),
('USRS-ONNTEZDTNGIYV6R', 'USZBLFWQWIGQMSSTNPOX', '089028843577', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 12, NULL),
('USRS-PR6SSGMRFR3IQWK', 'LVYPRMFDJZYBGYTPZXND', '089505271545', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 48, NULL),
('USRS-Q9DAMYWHNYBLQ2A', 'XJDXSVRGBAURUKFJODZY', '089896513325', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 40, NULL),
('USRS-QLOSQNVHEIULWPV', 'VHHAUJMKKKYDPDKKXBYR', '089327713849', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 16, NULL),
('USRS-QOAKPAYGWJKRBVL', 'EDUERXVFHKDCGIAWHFCK', '089752384875', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 36, NULL),
('USRS-QZF22QPKLFOCAXY', 'LWRVKZMGFQDAFYWKNRQC', '089321429864', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 28, NULL),
('USRS-R4T2QOEBZJFJLZD', 'AIYKJEVYIMOUYKCOKHFZ', '089778052605', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 36, NULL),
('USRS-R8ZHQJWOUMFAFCD', 'NBUBWVINZQKEHODOHGLT', '089257170758', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 33, NULL),
('USRS-RBFWKQSIBRN998M', 'DSLJQXCRPJGCOCDUEIKR', '089035112998', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 26, NULL),
('USRS-ROMQMFQPDYX1XBI', 'VLTPGRWBCKSVHDYQSQPP', '089963521829', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 47, NULL),
('USRS-RSENDCFVN07T7Y7', 'OZNUKHILSQVZINRFMCXE', '089560764888', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 24, NULL),
('USRS-RSIGHJE97AWKY05', 'JPBZYILRPSJWFTGUFECK', '089663495971', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 13, NULL),
('USRS-RWQKQKONKLQARWJ', 'OKNSZIBYDTPWJBSQVUZF', '089999373825', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 58, NULL),
('USRS-S9ZJGKCUHBQPDBP', 'DLONJJPATNOZFWSLGWWZ', '089199513796', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 16, NULL),
('USRS-SDF4F91WYCLMWVN', 'IXXOMMUOVZSKZBCYJDVW', '089212192465', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 51, NULL),
('USRS-SIBGYKYHXYOUMKI', 'JAPOPDGHYUVSBGHZDTJK', '089794811534', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 26, NULL),
('USRS-SM1DPTXBO0FLQEA', 'GQYOIJUJUGAMDYHDPSKG', '089420165300', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 28, NULL),
('USRS-SNIOJTVRBHDNVZQ', 'LRXMVMLDCVQSKCKNXHQM', '089639329356', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 29, NULL),
('USRS-T5BIWEOWTB1WEJP', 'Rizky Abdillah ', '082333826883', 'LAKI LAKI', 'Sedang mencari teman mabar ML!!!', '16930250211693025021_949daf850e5d45350331.png', 23, '2023-08-23 23:28:04'),
('USRS-TBCWZ2ADEMK2U8M', 'URNVXXSCIBHBFQAYNUHS', '089675200284', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-TELYXBMV55K5RBJ', 'OHBOTAPJINVBZPZNLWQJ', '089705777963', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 10, NULL),
('USRS-U1B8KFHBWZBPIRG', 'SVPDRVDACSMBRBZEDNJI', '089842859289', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 7, NULL),
('USRS-U2A0DPTSUPVN25K', 'KMFMUZLEKVNLRRBKKRTK', '089136298886', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 24, NULL),
('USRS-UEQ1E3TDPMJNBSH', 'FBYTANNZTXSTXSAFSEDM', '089436654111', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 27, NULL),
('USRS-UFSTAUVGXDO7HYN', 'JWCZHTCLWIEBJDBLJMEB', '089944648789', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 44, NULL),
('USRS-UTCUVBAIO7QOCF7', 'ZRGQYLVGCSOYFGVWAEUJ', '089027524326', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 45, NULL),
('USRS-UVQ78B7A6A7MD2E', 'ZAIIWXEYRZJGQPJUJIKX', '089192583909', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 7, NULL),
('USRS-VKUS2BUUPXNIEMZ', 'ITOIVTVMYYYVQXOZGBJO', '089103188490', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 18, NULL),
('USRS-W0BEVCHJTZUYUVE', 'ZUCNIRTOSEBLCCVFLJNN', '089341070540', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 56, NULL),
('USRS-W2EGPWFMMRRDX6I', 'BQZSTWHDZZZINUOCOAZD', '089699099228', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 52, NULL),
('USRS-W84IL6WOLXZTWHF', 'VFKOFWGXPWQFTIYZLAHK', '089938923644', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 52, NULL),
('USRS-WF91IBUZIQXYJDH', 'PWMNDTSZTYJUGZLMTZNT', '089161056486', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 41, NULL),
('USRS-X7P96132YBENCLY', 'IDRBNNALEDOIGNCQGIPA', '089635598998', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 19, NULL),
('USRS-XPIZMO5FK8CFRT3', 'ZQPYAYYQXFIQWTKGMZKR', '089219654134', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 28, NULL),
('USRS-XS3ED25FKYTFBR5', 'FFHSEXRGPXFTRNCDYFPY', '089932913314', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 48, NULL),
('USRS-XSB8Q9RTSOLQUGW', 'KIUBUJGOWAOQXSTJEJVL', '089546386423', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 27, NULL),
('USRS-YEECAXHKXLUK7M4', 'YTXFFJZVNQQEHYRMUSVU', '089179012015', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 6, NULL),
('USRS-YENXXSVKJPJ0YAD', 'OHANQQHEOVFOWTPRXPYQ', '089488779246', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 43, NULL),
('USRS-YP58RJ3I6WLG3Q8', 'NMWZFLWFEMOBNCUTOMSM', '089986079890', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 19, NULL),
('USRS-ZEB9WQWXA7M81CK', 'PMVMRKJEQAOYJURUXJPQ', '089061312472', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 8, NULL),
('USRS-ZIJ2P6JFGFSMQDR', 'GDASSTRSZVROHJSJERWC', '089528431013', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 31, NULL),
('USRS-ZKITBG6QVJPHCJC', 'FKCYIYIQMGWVKYKAFWAI', '089583311792', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 6, NULL),
('USRS-ZNJERALDVNLNIPG', 'GZGDHSNGAPLHGWKXCHHL', '089469915191', 'PEREMPUAN', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 9, NULL),
('USRS-ZY1ZNAZPQPBKRQ3', 'RMCSIVLWVNQLSNKGRKMB', '089262650058', 'LAKI LAKI', NULL, '16920351341692035134_5ff36dcdb10ffa1fe6bf.png', 60, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `USERS`
--

CREATE TABLE `USERS` (
  `ID_USER` char(20) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `PASSWORD` varchar(60) NOT NULL,
  `TOKEN` varchar(180) DEFAULT NULL,
  `OTP` char(5) DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `USERS`
--

INSERT INTO `USERS` (`ID_USER`, `USERNAME`, `EMAIL`, `PASSWORD`, `TOKEN`, `OTP`, `CREATED_AT`) VALUES
('USRS-1TXMVVUZU7E9B8A', 'OFWVGWRTXE627', 'testmail93208@gmail.com', '$2y$10$YA6KeoICSoPxoYpAI2hyhuY56AKnVOz0NnjpWRw8Xp0VtFfhV.Sbe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-21XRJBRHXLU5H11', 'PXNXKJBYAW472', 'testmail97632@gmail.com', '$2y$10$suiwtQ8.JtYAmZod9sjGXOkZqouZL96usWFMJA9iwTJyaHhuevOKy', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-27QELP0JIRGFMVA', 'MHEMVCCANS620', 'testmail20729@gmail.com', '$2y$10$kjHVdGM1P3fDhcaNhXd7zeuuruwgbPeMXEZ61T3NrxEM2fgfhRylO', 'caoloqeVRZWBlOWnBwygfu:APA91bHfWoNvLE9n6oI3-yfNlJxsTc-kvw2XkHcv13u2aIhMrGeTUaZ2T4Jk1Y2R0iDUbMsMI2qTbw63_u1RcqLkvg9We9i9TjbK8X0hkxpatNPQgAuNi-eeUDls_b8Q-ZwnQ6C9spjd', NULL, '2023-08-16 15:25:51'),
('USRS-3GDRLSN7HOKVC2E', 'GVVFFRGVGO966', 'testmail68662@gmail.com', '$2y$10$MK5E0OECOndU.1k0PrzyruDh0cueY/RZY.Pooyz9Nt9fe47oorKqS', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-3VOIUAZRQM5D7SQ', 'REWUQDAGQM668', 'testmail27252@gmail.com', '$2y$10$pcwvhAy8BrkqAYSKz7OfXuvZQvrFRy9QdSGfUsoy790fTnlNnBDDe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-45APCIKVCYS2PTL', 'UGSWEMWJPB568', 'testmail99390@gmail.com', '$2y$10$YbUadtJ8ZsUZTzjinLX9yuUj6yDGcEv.Hv95EOv.83UuKVigepaam', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-4EY58QCE98HM3NL', 'BQDDUYFQKL922', 'testmail00473@gmail.com', '$2y$10$mz4VliMLilv6f.a6.6K8GOLy51x5RJNzDI2BMrYGQK8cdYlYMO/12', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-54EATO6DBBJCGRG', 'OCRZWRBTXQ285', 'testmail22591@gmail.com', '$2y$10$QC9Vt4vwJrSnUaizHJC0Y.RiNhBpYd/Y9LLdXyU4lrP6pUJfnWaLq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-66QKYWU46VS2CCC', 'WSITIYTCQC758', 'testmail81693@gmail.com', '$2y$10$tKAhWfWAfpsGlVoAXcTuMO1oE9dQezeh8tI39Ro/E05mh60SY6GHS', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-7PI8XLQZ0TKMONS', 'DOIISBMTPD688', 'testmail96217@gmail.com', '$2y$10$rYQ5ZIAnQMOakHyAIYswpuVBHDlmOYRfLLCz7wInkN.e1jsOSeJGK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-7YEIRHYSN2M1WIC', 'MMEUHXEHZM745', 'testmail56863@gmail.com', '$2y$10$UNq5FzGFEGmdc.dIu4dB0.vWhF4dsNL0wD7gqjLKpJBxc/aolkVXm', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-8YFANWOOPQ60QET', 'PRQZVFOGPY262', 'testmail39722@gmail.com', '$2y$10$4L2WYtujHA8ppztAs6EdHe1ehb601CY1jyWBGH.aDj2mHoO3JL8c.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-AEVQG3FDV0Y3DM6', 'IECZEKBQMT387', 'testmail97695@gmail.com', '$2y$10$wbgaK.HOCA13UclxV6cfruWbkj0WdHOE5S59iyLC5zZSKB9sY7nYC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-AIBLPPSMTCRK06A', 'HUCGABHWGR776', 'testmail42317@gmail.com', '$2y$10$zm6sblNqqoKDaSUpmSTH8e6uVUdoSlgVrZz/F1mmsvc44KXbmIDoC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ALR6WNLRSNEKZAB', 'XXUSMIHLJK222', 'testmail08479@gmail.com', '$2y$10$96QW2KucWTclEcDeg2N0Cu2Gr3i2kuRMS0WWBGiej0jNwdb3ZxZnO', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-B5HY9ETVPIF2X8F', 'QYOHDYPMOL692', 'testmail94843@gmail.com', '$2y$10$YtqY5Yr4/DnQYHkiO9XDneaDo3BiXLPdyQwv.5b8dMPBpjUBBQtDy', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-BAABXXSPS3MY5B1', 'BPLJQDDQCL832', 'testmail51227@gmail.com', '$2y$10$sx.Ktj3xnSb.jQbF5pziNe0NDCLxb38qG7BSemAEkBHVu9lDkTdXa', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-BU3IRXEITHMP6U3', 'EGPBXQPUXW457', 'testmail47152@gmail.com', '$2y$10$GbdGAFk2HQ50AkzZ3aMNyuv9DX6Io.ImPbYjw7qL5A0j1c82Y/77O', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-C89TCPVZUDQZ6Y3', 'JSOZTFUOCQ211', 'testmail80050@gmail.com', '$2y$10$Zt.ChPXFKXquGLtSKhEezOhcXus.KerJ7GdOLG.ff5tA9Qk3LimvK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-CBR0IYLZS7UINWG', 'XEXADUYVQM823', 'testmail17529@gmail.com', '$2y$10$B3OhOY6C/Vhkg6rfQR3Ya.70sSmCrqo3.34aeFr4bUtUC74iw1f/e', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-CKOA3Q9T0L3QFYF', 'DLKMTNFQPN972', 'testmail24224@gmail.com', '$2y$10$a2raELv2GzXzLsL13111q..8sar2yRvZCqDduxIrxliro82mDUUuO', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-CM1O0VLGKLBCW92', 'TCDFIIHXNV163', 'testmail63335@gmail.com', '$2y$10$lC4B4IdPrCe9kCVC.RdCueWhJ.ckkpBD7XAiG.J3oqX4xesX5mD1W', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-CQJ4VNAOFQT5WCK', 'ZKWETBAXWH471', 'testmail70476@gmail.com', '$2y$10$9wDJgGYEWFwGDgVWMujtuuzoZtnk6mQNiWb8nbXssXGOP7WgJ1Pvq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-CZZYKMUCFQRYR7N', 'UBWBFAIULC786', 'testmail36714@gmail.com', '$2y$10$JCYc8QGf2em9A579eZxeEey2sZKhZUujGz1.WCHZ8Yg6OLOkraS/q', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-DLC4AQSJ0MSMLIG', 'VNJQBENCOZ322', 'testmail86737@gmail.com', '$2y$10$Ztr.htDKj3xm0UfqNKbWw.MVNbQD4ytvsW7s.yyNpx9d.Xjpbb3H6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-DURPMA7MHMQBMBG', 'XVTRZATFXF965', 'testmail29160@gmail.com', '$2y$10$.QY4OhZun2227wfEaUjjNen9uuK7BpKr5ZXTDOULdXi3jMYCV9lPS', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-EDM6TV3S11D9IAS', 'NTENNIIFFM634', 'testmail28640@gmail.com', '$2y$10$xfA6tS.zfTdrlD09r09LD.4nVYXowAXTB/ZUxQGQVzjCob1TUCosC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-EWXZDXF8WJLXKCL', 'QLWLZRCLVD377', 'testmail83967@gmail.com', '$2y$10$nGxdyzqExNmmo//6oMj9g.MH9KpYnG3WAG6ZLkuyVR7whcIiYpDyK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-FCIOSOV0Z7JTGAL', 'AYVCMSAJTQ844', 'testmail61636@gmail.com', '$2y$10$J/pQy6.dz0zJfEFhdjrAg.LXa.ywlTRtbMcn2fSmdA3kSRlCHECqS', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-FEZ7IRLU2CWUL75', 'OMMWUADYNS892', 'testmail31624@gmail.com', '$2y$10$VD6tT6M6XgLxZB.7QRORn.mE4XWRivcBhhXysfpZgKSSo9NW0jIY6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-FGFBKLWMSS1M03R', 'VOYMGLTDFW590', 'testmail06753@gmail.com', '$2y$10$bDRM5BmXqxdDFw708M9T6uiC9wR1yVkW8sXrrgiUy0Azb8bFhoJE6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-FRSXJFIMBRL212V', 'RGTZEADUAW181', 'testmail75079@gmail.com', '$2y$10$O4Ps4ePi.viDaanPGacHs.YnC96NlVYid/ZZtqGs14mz6SoNrgbHe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-GBGBYZRKMHBLJSO', 'CITRJBSCEC608', 'testmail14612@gmail.com', '$2y$10$WGGQNkMP.jDveeVjK2lAce6qJV.hehIewOEaDHV6/Lz4lIZJTObOK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-GDLUX5L9H6PT7M2', 'ZMOLBFGKSD706', 'testmail51901@gmail.com', '$2y$10$X1G7YRt6NtmIByjuwd4ZheEINeQ4OJ0BROueGgGLLlLMHHONkurAK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-GKFVZMBQMHXQEPO', 'SJIJWMPPPY771', 'testmail43904@gmail.com', '$2y$10$.gsje48hy8D5ZGEhW1mr/eU5sE1QVSPnqV4LywolIec1AxnxTeJUC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-GTYOM1CKSUZU1ZY', 'VKIKBYIRBP932', 'testmail50781@gmail.com', '$2y$10$y4UueNfzLbMGAByDzvBeIeSK0r7M8NXOcMW5at.2ttXwt4ZzfhxLe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-HDDY9VYYSAVSK1B', 'LLDCZEUPIO917', 'testmail35486@gmail.com', '$2y$10$uwDN1pUX.frJkMtTTqdVNONLvAUV4KEhfAf3dnEFJ7InYXHLH0wKm', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-HPYGJ5HR5UJL1ZP', 'KOPOSIYGEE581', 'testmail44927@gmail.com', '$2y$10$2Y8GRlkVzKprEL62NgASrulCOzSxcR84aguEG8AtdCDti43vzyuby', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-HUEOW7NXUDXM8HE', 'BMGBQBGGBS983', 'testmail60110@gmail.com', '$2y$10$H.lRAav4kntTGqhhjl1MHeG0uDuISUFWjDgTWsuf.BLrT41Zz0k3K', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-I9QWX1H4XO39VKO', 'URKDQAORZI975', 'testmail48911@gmail.com', '$2y$10$C4N.8Wy1rQT33H.xffB3FuWFgCJFjHqVRR3RX7RdnYyAAaj.dhc1u', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-IDEG6LS7E5CJTHA', 'UMQKJBIXTK973', 'testmail37134@gmail.com', '$2y$10$s8MTYz.bNSmLQKytFo4LaO3vNe8Gd9B64p.v9Z3GlTcXbPAby62tC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-IF1LVNXVJI6MNJC', 'AFCPEKWRMW332', 'testmail44280@gmail.com', '$2y$10$NrFbdhBTeLmv3TvQRXc7ge3SXqXA2IFnZwEnVM.LFULIq5/gn6c8C', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-IIOSZF73JJFQ6TW', 'FHEEAYFJJX845', 'testmail51680@gmail.com', '$2y$10$h36hGRUyMBmIEzdY4q6cvOvIeu.sFj5DAQ84MNJjJ8wK.gfrcVib.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-JPADIWFZFNBMOWR', 'GDYBYTKYGI514', 'testmail02484@gmail.com', '$2y$10$IyLhOmxfo1jiBzzknR/cCeVV0Uk7uSQcN7sIGc4XawmbgiIOnz72O', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-JPTHECTSO7EVXCC', 'VQODCDDAEF177', 'testmail36002@gmail.com', '$2y$10$O9FSybSOrNTDpphrLDCZJeeTvAuxoka0q4HYKdKG/NHVind4TpmkS', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-JYGOHJHVUWKWGBL', 'VUXIDVEMAL453', 'testmail45068@gmail.com', '$2y$10$bwIfWnSq6zUwnTXlCKkJT.KSdMnximt2.jRq6Ahw5Zg0Z3bmgkYwa', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-K3OWW3GSTTHTKFP', 'NDCMTZUPLZ667', 'testmail29500@gmail.com', '$2y$10$sH/KXiw0DlMNOT5yBaILG.uY75TCFo9Yx//XmHXCpLn53DsmuDFoi', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-K8QOAMBGQZPJFSN', 'BGBPAFYMFX476', 'testmail59409@gmail.com', '$2y$10$Ltf99lA7.5ARcMzip3xanO.vF1PadFjzLC5ZUFVwuMgZDtgtcjzxm', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-KU23AWTK1KT9IB4', 'NBRPGHAGHP935', 'testmail84506@gmail.com', '$2y$10$0RXyBEMOBEjAYV7pShCPyus8osALyF2UQwklWqRKucPe5mp49vHtm', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-KWE6S8WFY2MVSGZ', 'LXAARNQSIZ012', 'testmail74103@gmail.com', '$2y$10$RjnXSNrmUWAyjEd8nO0U7u2iktMa.jiGnb2i.E42C4WgYk8RKCKQq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-LBKSFXILWRIFW60', 'HNCYBFFPJR592', 'testmail11230@gmail.com', '$2y$10$rIXMIUQgkaOhVv8eyYs9Fu78LdfNJ1v.ac1AFY10wghR2/96Ysv76', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-LOLWQQK4INFH3QT', 'MTUVIVKNPO803', 'testmail47035@gmail.com', '$2y$10$Pr2OVD5uY8nYcV7uXsrdjOqgi3kwyxFGooyYestVZBsywwN42X6ii', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ML70PYUK8YMF7IQ', 'PDLLOIMHZK650', 'testmail74136@gmail.com', '$2y$10$pa2kyRzREpzs5oyoSas4NuSVZPtl6wcVeLe4oqUFFqgKi9jkJvvs.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-MV1GIBQFT7BQP9N', 'ACZUDFENUK352', 'testmail05927@gmail.com', '$2y$10$ez7t6iDNYK0WAPRQqCVoE.DgiYjC.H.ZQUazAD3qfZGKdTDjpxnNe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-NMEKYNSTLTFW4JK', 'OISTYSPDQZ887', 'testmail11179@gmail.com', '$2y$10$p8WEwa3rgnpfn0Ud4tLatu8tgbyp77W8p1u5zUfpy8Sh.T2OZ71eu', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-NZOKMYJLURN37CV', 'KUFXEULNOT878', 'testmail65235@gmail.com', '$2y$10$47oGVOMv0VO1DBW4OSoSpee1XV.zg/28e.0HVxoKRv9YiOeId0EXG', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-OJ1ODV7DLF6USHE', 'SCWOMAOUML280', 'testmail64793@gmail.com', '$2y$10$.03bbx1lI0XPbZnBYrkHVO2e.RM8Qxq922QPeaZFyFokUxgZL3vMe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ONNTEZDTNGIYV6R', 'NVSWOTEIZH036', 'testmail92037@gmail.com', '$2y$10$F9HE7KKhg.tGbVajTK3l0e3jl9Wh6itEzwfmaZ3x41ndqXniT59GK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-PR6SSGMRFR3IQWK', 'DLQCSZECBU128', 'testmail12081@gmail.com', '$2y$10$CHfmvpEhSC8QrG/l4MHAgOzo2OCjBghW7xU.mvfENFq2l9mg6QvKW', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-Q9DAMYWHNYBLQ2A', 'AXOKEHOHXS362', 'testmail79778@gmail.com', '$2y$10$Xxcq0zFIgs7WXwYRDGbaBePvSPl5t6NVvhz9PXmm1ohTVe.7QSwce', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-QLOSQNVHEIULWPV', 'SAZHEOXMUJ022', 'testmail91352@gmail.com', '$2y$10$gpvXdcHBsi7zR9GxXZFAxeuU0tu1PrMbBHEUTFCMtnC9qkiky3as2', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-QOAKPAYGWJKRBVL', 'WQIWIVOUKG901', 'testmail42634@gmail.com', '$2y$10$LFn9wZEFlJM4yHeBZP1N7u54FaPWRQ8UEHw/SushIAsAkqM2dINs.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-QZF22QPKLFOCAXY', 'PJCFRSGACN413', 'testmail83768@gmail.com', '$2y$10$0PvCvdtOtclmdExY0zl7gu1qQUo6ZSRPVU.jHo9PLiezTBL24GgOq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-R4T2QOEBZJFJLZD', 'KLLJNAXCGE275', 'testmail55136@gmail.com', '$2y$10$oJWfn22ZPbOEHigaSuPVMOHFPXupNHC6C6M5ATO7lQp7PdsVC.4Na', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-R8ZHQJWOUMFAFCD', 'MGJAWBZEXL585', 'testmail58333@gmail.com', '$2y$10$OuxO9mHdBuR7R8jvYBUwBuI5lGIMhmpMtfNaKX5Lhm8eA6wg8NP/K', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-RBFWKQSIBRN998M', 'MZYVPGNBMC504', 'testmail38522@gmail.com', '$2y$10$NeC9VFTPO9pIMhaR8rpv4OpEuDBdFSdxG3Y/Ls7okgtaWGSAjwWx2', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ROMQMFQPDYX1XBI', 'OCVXYPMVGX763', 'testmail76248@gmail.com', '$2y$10$aZElPeQj7obH1KW9gzhAnO7XRr9lzdFsKHKcdlwl0fJFnDgpeAxyK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-RSENDCFVN07T7Y7', 'ZZPRBDZFDL591', 'testmail74073@gmail.com', '$2y$10$JyzfGNLNVGEPj93t/FtfuuI.9cVfZ2TisBcy7E5cpWrY6XbIsgvIe', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-RSIGHJE97AWKY05', 'NPGKMDOXXN127', 'testmail94277@gmail.com', '$2y$10$N7JThj6E/81dZulXeDGryeME4/Itces8WxuHTo7OR/hEkLf05qER6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-RWQKQKONKLQARWJ', 'BDLBQUGUVX175', 'testmail77937@gmail.com', '$2y$10$JidOVSwm.uM2PT3MF1wGQumSbQ/tmuCIrAOq0hDDZ.2Ih9LaHrWe6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-S9ZJGKCUHBQPDBP', 'YIWTYQOSZR826', 'testmail77885@gmail.com', '$2y$10$agU8XUwKczEw933pMlirKe1JoeSyYj.6JlfKbAomaZwQa4FNsHW3S', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-SDF4F91WYCLMWVN', 'DISKJPDMQT817', 'testmail67264@gmail.com', '$2y$10$vMXXVIwUsRdVNEhg26gNbeLqx0iXpNRmnXXv.JOLz/mXmbWc24UkG', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-SIBGYKYHXYOUMKI', 'KUEAQXMHTF400', 'testmail95714@gmail.com', '$2y$10$7fSm8CIbl8FkG4fuifgOruSJV/30X/yriUusGr2Y/mgAPqN8BbI1.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-SM1DPTXBO0FLQEA', 'PJOLMCAJSM477', 'testmail94550@gmail.com', '$2y$10$vyTNeVv6RbHQ0af3IXn8U.DwsGvu7Inuv9ca.a2XUVvU.knYx86qu', 'fMVN7ZkQTByoANESaRRcvZ:APA91bF77oDN9nidXzoXQIkSf1ox2evbWf0nLShzFE6nWs5BP_01eHQzZe0D3Hlep8dNlp7Olv3_PRylq6EnCxyFOGSeECjwt8A00Ve6veX7vNbSCiIDXsas-F3ghzbQE9JZrs3zIgkU', NULL, '2023-08-16 15:25:51'),
('USRS-SNIOJTVRBHDNVZQ', 'INMUUYWALR954', 'testmail42774@gmail.com', '$2y$10$OyoCHsADALOVMljSnfPIHOzJ8v6vNifWLtlIJoIVkBQj3Z87A3LdC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-T5BIWEOWTB1WEJP', 'RIZKY', 'anjay7676@gmail.com', '$2y$10$MtGYI.FMPQEo0GRD6ZveweVsnmEU39pnkP4pryNsqy1F8xPZ/obBq', 'fotpcl1tQy259vERDtx7qd:APA91bHZD4OED2nSriuPB8k1_033dLIWVXUCztUP9gdJz-S74nF5G_Jqc7k3RexjbvElmeA806PoQIJWQC5f1pxirQRiNAsn2EtZom17arlYMCjmD1NJhh_Du0oCOT0Yww66rwOuGrvr', '89609', '2023-08-11 20:13:31'),
('USRS-TBCWZ2ADEMK2U8M', 'DFJHVMWGBZ507', 'testmail12087@gmail.com', '$2y$10$WznKKaOlHIXWyKvpJ/e46O2xQiFn.Mlr39hf4Hj2CdSB7ZGjBLu6G', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-TELYXBMV55K5RBJ', 'BMNBGEQPWM147', 'testmail52319@gmail.com', '$2y$10$0wBb2ijGdS6HxV5x6wLVhO9M72LCXXeaq6VNGiEvfyh8cQ5Z52a3W', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-U1B8KFHBWZBPIRG', 'QPWIMJGJIA984', 'testmail20696@gmail.com', '$2y$10$MDN86vWwK19bmCxMmHUsu.IaqGNqChlOElix8dMqz95urxwfEGbzm', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-U2A0DPTSUPVN25K', 'GSJLJYZPNH247', 'testmail79727@gmail.com', '$2y$10$r7yjfwwwUN1lIQGtgAFZpumyv0od9OYWuEiI.FnIqVSW/IVxZpCRW', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-UEQ1E3TDPMJNBSH', 'DQWLHMFONC346', 'testmail34542@gmail.com', '$2y$10$95xVh4tx3Q2G/8D4q2uoJ.qyvzKHZckG4PnIPfkT7kuWU/OfX.2Se', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-UFSTAUVGXDO7HYN', 'MCYAGHYOJU741', 'testmail23122@gmail.com', '$2y$10$tmZjfhxo1aGJ98ZyNKYW0uiJlhcK5w/P4q3Lyp2yy8I6osH9LNRjq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-UTCUVBAIO7QOCF7', 'NFOKSPSBCK715', 'testmail84717@gmail.com', '$2y$10$7UZGWRwDjG4Bk.5KzaxZCuuOsN2atH/WTU42X6hFAd4EPY0jLBfMG', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-UVQ78B7A6A7MD2E', 'DEJDLRWDPL295', 'testmail58921@gmail.com', '$2y$10$w1WFC0WFA/5.ftHAHRK/xeMKww6Fs2W0.i.SbXMqL3R3Y3nEzliZa', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-VKUS2BUUPXNIEMZ', 'MKGWCLTHZB671', 'testmail92886@gmail.com', '$2y$10$mcZs.PjQBklgJ5USCZUwNuWVyklxeUwhO.XTY/8ztnjVd/DEBw/Fa', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-W0BEVCHJTZUYUVE', 'DKXFSJWDYO534', 'testmail09861@gmail.com', '$2y$10$BTFtOLiTIFD6Jv4sVP3e2OqDcYl2x5lHqAPFF.lfrzSvICrod6oim', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-W2EGPWFMMRRDX6I', 'BPZZQFKZKP625', 'testmail76018@gmail.com', '$2y$10$T3boQxl.FjXyvChEXymileJ6y1a4pvljBdbLScVLMnvETs7/aHfGO', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-W84IL6WOLXZTWHF', 'FBHTGBCOBM439', 'testmail72206@gmail.com', '$2y$10$ppq7WEH6nRC5rsA0OdLn3.2hCY4XAH8Gk8a7QPoa/Ov66A7AeQ/e6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-WF91IBUZIQXYJDH', 'YUVMTHGYLO573', 'testmail60151@gmail.com', '$2y$10$kncICUO3L7WW4TvVKv1oyOAI9LQKaMqW743Mv7RprLHHCVbjqDfYq', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-X7P96132YBENCLY', 'RBEGPOFDDC398', 'testmail59054@gmail.com', '$2y$10$J/O4JO.pjBINyhEp//vHHO7GvhFReyj8hZMA2euTplsuhgv6/EZTC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-XPIZMO5FK8CFRT3', 'UGTPBPUOCR386', 'testmail35698@gmail.com', '$2y$10$hYD.F5wsNF2K77IibMOPhOORjk9saWL9O3ayJrgh7gSX1Z1nekQ8.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-XS3ED25FKYTFBR5', 'FQOVNDIPOT639', 'testmail49998@gmail.com', '$2y$10$a3yMleKRRnm8/mVblidTQ.1626Kt5Mmkhv92xVafLlrUjPc6hD2hu', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-XSB8Q9RTSOLQUGW', 'ZUITLUADGN728', 'testmail72149@gmail.com', '$2y$10$P/Aoj4S8M2ysdTzAvtMHZu0h/zZaicC0e05xfDjlZ3JNrNPTeH.d.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-YEECAXHKXLUK7M4', 'QSDKIHFRHX791', 'testmail97163@gmail.com', '$2y$10$Ic40leSJkjE4lKFGNtRwXek9W2.4xvbBiWCtvkJv7atCH0NULXTbG', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-YENXXSVKJPJ0YAD', 'WXKNQRCNNY254', 'testmail40556@gmail.com', '$2y$10$bGdU.jgh41d/W2Qt8wVYHOfXWbaQPtQlp3dJcoYSkelnIr85DDa/K', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-YP58RJ3I6WLG3Q8', 'CIMWEWCLWN021', 'testmail19184@gmail.com', '$2y$10$ZrBxpsfN2lZDvjAQw0NH3.UsgNjQ65kI8QJsKONT2e1K/P6DG5VL6', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ZEB9WQWXA7M81CK', 'IEDKQSRHKR718', 'testmail70801@gmail.com', '$2y$10$viHdHjgJZLLz3ZyBTVcU7uY.MlvaBcWnX4BiFqWElPN/xVECLPdc.', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ZIJ2P6JFGFSMQDR', 'DCOVBDPUOH382', 'testmail65966@gmail.com', '$2y$10$d5n0oNnJTiDVLlXMn91x4eq//JZlRMsLuxV.tXEzmDxPdZOxhw8LC', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ZKITBG6QVJPHCJC', 'NNLZQBVHKO975', 'testmail41652@gmail.com', '$2y$10$zp1T6ZmgbkZmp35COPScxel5GqpYlKbUl8IXRhK3CmJfS.tM9j8Cu', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ZNJERALDVNLNIPG', 'THKKXNAHQK558', 'testmail61390@gmail.com', '$2y$10$5aeV3FDfoI70NRalEOdemuXJoepYhs71X/6pZJn9emNc.55/h6izK', NULL, NULL, '2023-08-16 15:25:51'),
('USRS-ZY1ZNAZPQPBKRQ3', 'VMAQCITDRX607', 'testmail42672@gmail.com', '$2y$10$V4Er6DC87h5wzGtMhFvUWeNmtjXuJXAXwG3dXh4V4T3N7X3FMYNSG', NULL, NULL, '2023-08-16 15:25:51');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `CHAT_ROOMS`
--
ALTER TABLE `CHAT_ROOMS`
  ADD PRIMARY KEY (`ID_CHAT_ROOM`);

--
-- Indeks untuk tabel `DETAIL_CHATS`
--
ALTER TABLE `DETAIL_CHATS`
  ADD PRIMARY KEY (`ID_CHAT`);

--
-- Indeks untuk tabel `EVENTS`
--
ALTER TABLE `EVENTS`
  ADD PRIMARY KEY (`ID_EVENT`);

--
-- Indeks untuk tabel `ID_GAMES`
--
ALTER TABLE `ID_GAMES`
  ADD PRIMARY KEY (`ID_IDGAME`);

--
-- Indeks untuk tabel `LOCATIONS`
--
ALTER TABLE `LOCATIONS`
  ADD PRIMARY KEY (`ID_LOCATION`);

--
-- Indeks untuk tabel `PROFILES`
--
ALTER TABLE `PROFILES`
  ADD PRIMARY KEY (`ID_PROFILE`);

--
-- Indeks untuk tabel `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`ID_USER`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
