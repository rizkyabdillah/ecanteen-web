<?php

namespace Config;

use App\Controllers\admin\Auth;
use App\Controllers\admin\Dashboard;
use App\Controllers\admin\Events;
use App\Controllers\admin\Pengguna;
use App\Controllers\api\ApiAccount;
use App\Controllers\api\ApiAuth;
use App\Controllers\api\ApiBlock;
use App\Controllers\api\ApiCanteen;
use App\Controllers\api\ApiCariTeman;
use App\Controllers\api\ApiChat;
use App\Controllers\api\ApiEvent;
use App\Controllers\api\ApiIDGame;
use App\Controllers\api\ApiLocation;
use App\Controllers\api\ApiMenu;
use App\Controllers\api\ApiProfile;
use App\Controllers\api\ApiRegister;
use App\Controllers\api\ApiTransaction;

$routes = Services::routes();

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// $routes->get('/', 'Home::index');

// ---------------------------------------------------------
// ADMIN ROUTES
// ---------------------------------------------------------
// $routes->group('/',  ['filter' => 'not_auth_admin_filter'], function ($routes) {
//     $routes->get('',                                            [Dashboard::class, 'index'],            ['as' => 'dash-index']);
//     $routes->group('pengguna',  function ($routes) {
//         $routes->get('',                                        [Pengguna::class, 'index'],             ['as' => 'pengguna-index']);
//     });
//     $routes->group('events',  function ($routes) {
//         $routes->get('add',                                     [Events::class, 'indexAdd'],            ['as' => 'events-add-index']);
//         $routes->get('(:any)',                                  [Events::class, 'indexEdit'],           ['as' => 'events-edit-index']);
//         $routes->get('',                                        [Events::class, 'index'],               ['as' => 'events-index']);
//         $routes->post('',                                       [Events::class, 'insert'],              ['as' => 'events-insert']);
//         $routes->put('(:any)',                                  [Events::class, 'update'],              ['as' => 'events-update']);
//         $routes->delete('(:any)',                               [Events::class, 'delete'],              ['as' => 'events-delete']);
//     });
// });

// $routes->get('bcrypt/(:any)',                                   [Auth::class, 'bcrypt']);
// $routes->get('auth',                                            [Auth::class, 'index'],                 ['as' => 'auth-index', 'filter' => 'auth_admin_filter']);
// $routes->get('logout',                                          [Auth::class, 'logout'],                ['as' => 'logout']);
// $routes->post('auth',                                           [Auth::class, 'auth'],                  ['as' => 'auth']);

// ---------------------------------------------------------
// API ROUTES
// ---------------------------------------------------------
$routes->group('api',  function ($routes) {

    $routes->group('canteens',  function ($routes) {
        $routes->get('',                                        [ApiCanteen::class, 'show']);
        $routes->get('detail',                                  [ApiCanteen::class, 'showDetail']);
        $routes->delete('',                                     [ApiCanteen::class, 'delete']);
        $routes->post('',                                       [ApiCanteen::class, 'insert']);
        $routes->post('update',                                 [ApiCanteen::class, 'update']);
    });

    $routes->group('auths',  function ($routes) {
        $routes->post('',                                       [ApiAuth::class, 'login']);
        $routes->post('register',                               [ApiAuth::class, 'register']);
    });

    $routes->group('menus',  function ($routes) {
        $routes->get('',                                        [ApiMenu::class, 'show']);
        $routes->post('',                                       [ApiMenu::class, 'insert']);
        $routes->post('update',                                 [ApiMenu::class, 'update']);
        $routes->delete('',                                     [ApiMenu::class, 'delete']);
    });

    $routes->group('profiles',  function ($routes) {
        $routes->get('',                                        [ApiProfile::class, 'show']);
        $routes->get('full',                                    [ApiProfile::class, 'showFull']);
        $routes->put('status',                                  [ApiProfile::class, 'updateStatusBuka']);
        $routes->post('user',                                   [ApiProfile::class, 'updateProfileUser']);
        $routes->post('canteen',                                [ApiProfile::class, 'updateProfileKantin']);
    });

    $routes->group('accounts',  function ($routes) {
        $routes->get('',                                        [ApiAccount::class, 'show']);
        $routes->get('merged',                                  [ApiAccount::class, 'showMerged']);
        $routes->post('',                                       [ApiAccount::class, 'insert']);
        $routes->put('',                                        [ApiAccount::class, 'update']);
        $routes->delete('',                                     [ApiAccount::class, 'delete']);
    });

    $routes->group('transactions',  function ($routes) {
        $routes->get('riwayat',                                 [ApiTransaction::class, 'showRiwayat']);
        $routes->get('detailp',                                 [ApiTransaction::class, 'showDetailPembeli']);
        $routes->get('tagihan',                                 [ApiTransaction::class, 'showTagihan']);
        $routes->get('pesanan',                                 [ApiTransaction::class, 'showPesananKantin']);
        $routes->get('rekap',                                   [ApiTransaction::class, 'showRekap']);
        $routes->get('drekap',                                  [ApiTransaction::class, 'showDetailRekap']);
        $routes->post('',                                       [ApiTransaction::class, 'insert']);
        $routes->post('tagihan',                                [ApiTransaction::class, 'unggahTagihan']);
        $routes->put('status',                                  [ApiTransaction::class, 'updateStatus']);
    });


    // $routes->group('events',  function ($routes) {
    //     $routes->get('(:any)',                                  [ApiEvent::class, 'show']);
    //     $routes->get('',                                        [ApiEvent::class, 'show']);
    // });

    // $routes->group('users',  function ($routes) {
    //     $routes->post('login',                                  [ApiAuth::class, 'login']);
    //     $routes->post('register',                               [ApiRegister::class, 'register']);
    //     $routes->post('otp/(:any)/send',                        [ApiAuth::class, 'otpSend']);
    //     $routes->post('otp/(:any)/verif',                       [ApiAuth::class, 'otpVerif']);
    //     $routes->put('pass/(:any)/reset',                       [ApiAuth::class, 'passReset']);
    //     $routes->put('pass/(:any)/update',                      [ApiAuth::class, 'passUpdate']);
    // });

    // $routes->group('profiles',  function ($routes) {
    //     $routes->post('(:any)/foto',                            [ApiProfile::class, 'updateFoto']);
    //     $routes->put('(:any)/status',                           [ApiProfile::class, 'updateStatus']);
    //     $routes->get('(:any)/showfull',                         [ApiProfile::class, 'showFull']);
    //     $routes->get('(:any)',                                  [ApiProfile::class, 'show']);
    //     $routes->put('(:any)',                                  [ApiProfile::class, 'update']);
    // });

    // $routes->group('locations',  function ($routes) {
    //     $routes->get('(:any)',                                  [ApiLocation::class, 'show']);
    //     $routes->post('(:any)',                                 [ApiLocation::class, 'store']);
    // });

    // $routes->group('idgames',  function ($routes) {
    //     $routes->get('(:any)/detail',                           [ApiIDGame::class, 'detail']);
    //     $routes->get('(:any)',                                  [ApiIDGame::class, 'show']);
    //     $routes->post('(:any)/update',                          [ApiIDGame::class, 'update']);
    //     $routes->post('(:any)',                                 [ApiIDGame::class, 'store']);
    //     $routes->delete('(:any)',                               [ApiIDGame::class, 'delete']);
    // });

    // $routes->group('friends',  function ($routes) {
    //     $routes->get('(:any)/profile',                          [ApiCariTeman::class, 'profile']);
    //     $routes->get('(:any)/show',                             [ApiCariTeman::class, 'showFriend']);
    //     $routes->get('(:any)',                                  [ApiCariTeman::class, 'show']);
    //     $routes->post('(:any)/add',                             [ApiCariTeman::class, 'add']);
    // });

    // $routes->group('chats',  function ($routes) {
    //     $routes->get('',                                        [ApiChat::class, 'showChat']);
    //     $routes->get('room',                                    [ApiChat::class, 'showChatRoom']);
    //     $routes->post('send',                                   [ApiChat::class, 'send']);
    //     $routes->delete('',                                     [ApiChat::class, 'delete']);
    // });

    // $routes->group('blocks',  function ($routes) {
    //     $routes->post('',                                       [ApiBlock::class, 'block']);
    // });
});

if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
