<?php

namespace App\Controllers;

use App\Models\CrudModel;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Model;
use Psr\Log\LoggerInterface;

class BaseController extends Controller
{
    /**
     * @var CLIRequest|IncomingRequest
     */
    protected $request;
    protected $validation;
    protected $model;
    protected $email;
    protected $notif;

    /**
     * @var array
     */
    protected $helpers = ['form', 'session', 'text'];

    protected function getPost($key = null)
    {
        if ($key == null) {
            return $this->request->getPost();
        }
        return $this->request->getPost($key);
    }

    protected function getGet($key = null)
    {
        if ($key == null) {
            return $this->request->getGet();
        }
        return $this->request->getGet($key);
    }

    protected function getRaw($key = null)
    {
        if ($key == null) {
            return $this->request->getRawInput();
        }
        return $this->request->getRawInput($key);
    }

    protected function getFile($key = null)
    {
        if ($key == null) {
            return $this->request->getFiles();
        }
        return $this->request->getFile($key);
    }

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->validation = \Config\Services::validation();
        $this->model = new CrudModel();
        $this->email = new EmailSender();
        $this->notif = new NotifSender();

        session();
    }
}
