<?php

namespace App\Controllers;

class EmailSender
{

    const EMAIL = 'admin@senpay.pw';
    const NAME = 'ADMIN VINZONE';
    const HEADER = 'OTP Perubahan Password!';

    public function email()
    {
        return \Config\Services::email();
    }

    public function send($receiver, $textBody)
    {
        $this->email()->setFrom(self::EMAIL, self::NAME);
        $this->email()->setTo($receiver);
        $this->email()->setSubject(self::HEADER);
        $this->email()->setMessage($textBody);
        if (!$this->email()->send()) {
            echo $this->email()->printDebugger(['headers']);
            return false;
        } else {
            return true;
        }
    }
}
