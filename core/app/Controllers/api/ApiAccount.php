<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiAccount extends BaseApi
{

    public function show()
    {
        $GET_DATA   = $this->getGet();

        $QUERY      = "SELECT * FROM ACCOUNTS WHERE ID_USER ='" . $GET_DATA['ID_USER'] . "' AND DELETED_AT IS NULL ORDER BY CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Data rekening kosong, silahkan tambahkan rekening!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function showMerged()
    {
        $GET_DATA   = $this->getGet();

        $QUERY      = "SELECT * FROM ACCOUNTS WHERE ID_USER ='" . $GET_DATA['ID_USER'] . "' AND DELETED_AT IS NULL GROUP BY NAMA_BANK ORDER BY CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Tidak ditemukan rekening!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function delete()
    {
        $GET_DATA   = $this->getGet();

        $UPDATE = $this->model->updateData('ACCOUNTS', ['DELETED_AT' => $this->getTimeStamp()], $GET_DATA);
        if (!$UPDATE) {
            return $this->setRespond('Gagal menghapus rekening!', null, 400);
        } else {
            return $this->setRespond('Berhasil menghapus rekening!');
        }
    }

    public function insert()
    {
        $RAW_DATA                  = $this->getRaw();
        $RAW_DATA['ID_ACCOUNT']    = 'ACC-' . strtoupper(random_string('alnum', 21));

        $INSERT = $this->model->insertData('ACCOUNTS', $RAW_DATA);

        if (!$INSERT) {
            return $this->setRespond('Gagal menambahkan rekening!', null, 400);
        } else {
            return $this->setRespond('Berhasil menambahkan rekening!');
        }
    }

    public function update()
    {
        $RAW_DATA   = $this->getRaw();
        $GET_DATA   = $this->getGet();
        
        $UPDATE = $this->model->updateData('ACCOUNTS', $RAW_DATA, $GET_DATA);

        if (!$UPDATE) {
            return $this->setRespond('Gagal mengubah rekening!', null, 400);
        } else {
            return $this->setRespond('Berhasil mengubah rekening!');
        }
    }
}
