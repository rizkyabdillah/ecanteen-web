<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiProfile extends BaseApi
{

    public function updateStatusBuka()
    {
        $GET_DATA = $this->getGet();
        $RAW_DATA = $this->getRaw();

        $UPDATE = $this->model->updateData('CANTEEN_PROFILES', $RAW_DATA, $GET_DATA);

        if (!$UPDATE) {
            return $this->setRespond('Gagal mengubah status', null, 400);
        } else {
            return $this->setRespond('Berhasil mengubah status!');
        }
    }

    public function updateProfileUser()
    {
        $POST_DATA      = $this->getPost();
        $GET_DATA       = $this->getGet();
        $FILE_FOTO      = $this->getFile('FOTO');

        $ID_USER        = $GET_DATA['ID_USER'];


        $DATA = [
            'EMAIL'     => $POST_DATA['EMAIL'],
        ];
        $this->model->updateData('USERS', $DATA, $GET_DATA);



        $DATA = [
            'NAMA_LENGKAP'  => ucfirst($POST_DATA['NAMA_LENGKAP']),
            'TELEPON'       => $POST_DATA['TELEPON'],
        ];

        if ($FILE_FOTO != null) {
            $NAMA_FOTO      = time() . $FILE_FOTO->getRandomName();
            $DATA['FOTO'] = $NAMA_FOTO;
            $FILE_FOTO->move(FCPATH . 'assets/foto/', $NAMA_FOTO);
        }
        $this->model->updateData('PROFILES', $DATA, ['ID_PROFILE' => $ID_USER]);

        return $this->setRespond('Berhasil mengubah profile pengguna!');
        
    }
    
    public function updateProfileKantin()
    {
        $POST_DATA      = $this->getPost();
        $GET_DATA       = $this->getGet();
        $FILE_BANNER    = $this->getFile('BANNER');

        $ID_USER        = $GET_DATA['ID_USER'];

        if ($FILE_BANNER != null) {
            $NAMA_BANNER    = time() . $FILE_BANNER->getRandomName();
            $POST_DATA['BANNER'] = $NAMA_BANNER;
            $FILE_BANNER->move(FCPATH . 'assets/foto/', $NAMA_BANNER);
        }
        $this->model->updateData('CANTEEN_PROFILES', $POST_DATA, ['ID_PROFILE' => $ID_USER]);


        return $this->setRespond('Berhasil mengubah kantin!');
    }

    function showFull() {
        $GET_DATA   = $this->getGet();

        $QUERY      = "SELECT B.ID_USER, A.NAMA_LENGKAP, A.TELEPON, B.USERNAME, B.EMAIL, C.NAMA_CANTEEN, CONCAT('" . base_url() . "assets/foto/', C.BANNER) AS BANNER, CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO, C.NOMOR_CANTEEN, C.JAM_BUKA, C.JAM_TUTUP, C.STATUS_BUKA FROM PROFILES AS A INNER JOIN USERS AS B ON(A.ID_PROFILE = B.ID_USER) INNER JOIN CANTEEN_PROFILES AS C ON(C.ID_PROFILE = A.ID_PROFILE) WHERE B.ID_USER ='" . $GET_DATA['ID_USER'] . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data profile tidak ditemukan!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    function show() {
        $GET_DATA   = $this->getGet();

        $QUERY      = "SELECT B.USERNAME, B.EMAIL, A.NAMA_LENGKAP, A.TELEPON, CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO FROM PROFILES AS A INNER JOIN USERS AS B ON(A.ID_PROFILE = B.ID_USER) WHERE B.ID_USER ='" . $GET_DATA['ID_USER'] . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data profile tidak ditemukan!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }


}
