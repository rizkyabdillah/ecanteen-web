<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiMenu extends BaseApi
{

    public function show()
    {
        $GET_DATA           = $this->getGet();
        $ID_CANTEEN_PROFILE = $GET_DATA['ID_CANTEEN_PROFILE'];

        $QUERY = "SELECT ID_MENU, NAMA_MENU, HARGA, CONCAT('" . base_url() . "assets/foto/', GAMBAR) AS GAMBAR, STOK FROM MENUS WHERE ID_CANTEEN_PROFILE ='" . $ID_CANTEEN_PROFILE . "' AND DELETED_AT IS NULL ORDER BY CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Menu anda kosong, silahkan tambahkan menu!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function insert()
    {
        $POST_DATA              = $this->getPost();
        $FILE_GAMBAR            = $this->getFile('GAMBAR');
        $NAMA_GAMBAR            = time() . $FILE_GAMBAR->getRandomName();

        $POST_DATA['ID_MENU']   = 'MENUS-' . strtoupper(random_string('alnum', 19));
        $POST_DATA['GAMBAR']    = $NAMA_GAMBAR;

        $FILE_GAMBAR->move(FCPATH . 'assets/foto/', $NAMA_GAMBAR);

        $INSERT = $this->model->insertData('MENUS', $POST_DATA);
        if ($INSERT) {
            return $this->setRespond('Berhasil menambahkan menu!');
        } else {
            return $this->setRespond('Gagal menambahkan menu!', null, 400);
        }
    }

    public function update()
    {
        $GET_DATA       = $this->getGet();
        $POST_DATA      = $this->getPost();
        $FILE_GAMBAR    = $this->getFile('GAMBAR');

        $DATA_MENU                          = $this->model->getRowDataArray('MENUS', $GET_DATA);
        $POST_DATA['ID_MENU']               = 'MENUS-' . strtoupper(random_string('alnum', 19));
        $POST_DATA['ID_CANTEEN_PROFILE']    = $DATA_MENU['ID_CANTEEN_PROFILE'];


        if ($FILE_GAMBAR != null) {
            $NAMA_GAMBAR            = time() . $FILE_GAMBAR->getRandomName();
            $POST_DATA['GAMBAR']    = $NAMA_GAMBAR;
            $FILE_GAMBAR->move(FCPATH . 'assets/foto/', $NAMA_GAMBAR);
        } else {
            $POST_DATA['GAMBAR']    = $DATA_MENU['GAMBAR'];
        }

        $INSERT = $this->model->insertData('MENUS', $POST_DATA);
        $UPDATE = $this->model->updateData('MENUS', ['DELETED_AT' => $this->getTimeStamp()], $GET_DATA);
        if ($UPDATE) {
            return $this->setRespond('Berhasil mengubah menu!');
        } else {
            return $this->setRespond('Gagal mengubah menu!', null, 400);
        }
    }


    public function delete()
    {
        $GET_DATA   = $this->getGet();

        $UPDATE = $this->model->updateData('MENUS', ['DELETED_AT' => $this->getTimeStamp()], ['ID_MENU' => $GET_DATA['ID_MENU']]);
        if (!$UPDATE) {
            return $this->setRespond('Gagal menghapus menu!', null, 400);
        } else {
            return $this->setRespond('Berhasil menghapus menu!');
        }
    }
}
