<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiTransaction extends BaseApi
{
    public function insert()
    {
        $RAW_DATA       = $this->getRaw();
        $ID_TRANSACTION = strtoupper(random_string('alpha', 8)) . random_string('nozero', 12);
        $RAW_DATA['ID_TRANSACTION'] = $ID_TRANSACTION;

        $TEMP = json_decode($RAW_DATA['DATA_MENU']);
        unset($RAW_DATA['DATA_MENU']);

        $DATA_MENU = array();
        foreach ($TEMP as $VAL) {
            $VAL->ID_TRANSACTION = $ID_TRANSACTION;
            $this->model->updateDataEscape('MENUS', ['STOK' => 'STOK-' . $VAL->JUMLAH], ['ID_MENU' => $VAL->ID_MENU]);
            array_push($DATA_MENU, $VAL);
        }


        $ID_PENJUAL = $RAW_DATA['ID_PENJUAL'];
        unset($RAW_DATA['ID_PENJUAL']);
        $DATA_USER = $this->model->getRowDataColumnWhereArray('USERS', 'TOKEN', ['ID_USER' => $ID_PENJUAL]);

        $this->notif->sendNotif($DATA_USER['TOKEN'], 'Transaksi Baru!', 'Lihat, dan segera proses transaksi tersebut.');

        $this->model->insertData('TRANSACTIONS', $RAW_DATA);
        $this->model->insertDataBatch('DETAIL_TRANSACTIONS', $DATA_MENU);

        return $this->setRespond('Transaksi Berhasil!');
    }

    public function showRiwayat()
    {
        $GET_DATA   = $this->getGet();
        $ID_USER    = $GET_DATA['ID_USER'];

        $QUERY = "SELECT A.ID_TRANSACTION, DATE(A.CREATED_AT) AS CREATED_AT, CONCAT('" . base_url() . "assets/foto/', B.BANNER) AS BANNER, B.NOMOR_CANTEEN, B.NAMA_CANTEEN, (SELECT SUM(JUMLAH) FROM DETAIL_TRANSACTIONS WHERE ID_TRANSACTION = A.ID_TRANSACTION) AS JUMLAH, IF(ISNULL(A.ID_ACCOUNT), 'Cashless', 'Cash') AS PEMBAYARAN, (SELECT SUM(X.JUMLAH * Y.HARGA) FROM DETAIL_TRANSACTIONS AS X LEFT JOIN MENUS AS Y ON(X.ID_MENU = Y.ID_MENU) WHERE X.ID_TRANSACTION = A.ID_TRANSACTION) AS TOTAL, A.STATUS, A.ID_CANTEEN_PROFILE, B.ID_PROFILE AS ID_PENJUAL FROM TRANSACTIONS AS A LEFT JOIN CANTEEN_PROFILES AS B ON(A.ID_CANTEEN_PROFILE = B.ID_CANTEEN_PROFILE) WHERE A.ID_USER = '" . $ID_USER . "' ORDER BY A.CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Belum ada riwayat transaksi!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function showDetailPembeli()
    {
        $GET_DATA       = $this->getGet();
        $ID_TRANSACTION = $GET_DATA['ID_TRANSACTION'];

        $DATA_TRANSACTION = $this->model->getRowDataColumnWhereArray('TRANSACTIONS', [
            'TEMPAT_PENGANTARAN',
            'STATUS',
            'CATATAN',
            'WAKTU_DISIAPKAN',
            'ID_CANTEEN_PROFILE'
        ], $GET_DATA);

        $QUERY = "SELECT A.NAMA_CANTEEN, CONCAT('" . base_url() . "assets/foto/', A.BANNER) AS BANNER, B.NAMA_LENGKAP FROM CANTEEN_PROFILES AS A LEFT JOIN PROFILES AS B ON(A.ID_PROFILE = B.ID_PROFILE) WHERE A.ID_CANTEEN_PROFILE = '" . $DATA_TRANSACTION['ID_CANTEEN_PROFILE'] . "'";
        $DATA_KANTIN = $this->model->queryRowArray($QUERY);

        $QUERY = "SELECT A.NAMA_MENU, A.HARGA, B.JUMLAH FROM MENUS AS A RIGHT JOIN DETAIL_TRANSACTIONS AS B ON(A.ID_MENU = B.ID_MENU) WHERE B.ID_TRANSACTION ='" . $ID_TRANSACTION . "'";
        $DATA_PESANAN = $this->model->queryArray($QUERY);

        $DATA_TRANSACTION['DATA_KANTIN'] = $DATA_KANTIN;
        $DATA_TRANSACTION['DATA_PESANAN'] = $DATA_PESANAN;

        return $this->setRespond('Berhasil mendapatkan data!', $DATA_TRANSACTION);
    }

    public function showTagihan()
    {
        $GET_DATA       = $this->getGet();
        $ID_TRANSACTION = $GET_DATA['ID_TRANSACTION'];

        $QUERY = "SELECT B.ID_TRANSACTION, A.NAMA_BANK, A.NOMOR_REKENING, A.NAMA_PEMILIK, (SELECT SUM(X.JUMLAH * Y.HARGA) FROM DETAIL_TRANSACTIONS AS X LEFT JOIN MENUS AS Y ON(X.ID_MENU = Y.ID_MENU) WHERE X.ID_TRANSACTION = B.ID_TRANSACTION) AS TOTAL FROM ACCOUNTS AS A RIGHT JOIN TRANSACTIONS AS B ON(A.ID_ACCOUNT = B.ID_ACCOUNT) WHERE B.ID_TRANSACTION ='" . $ID_TRANSACTION . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data tidak ditemukan!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function unggahTagihan()
    {
        $GET_DATA       = $this->getGet();
        $ID_TRANSACTION = $GET_DATA['ID_TRANSACTION'];

        $FILE_BUKTI     = $this->getFile('BUKTI_PEMBAYARAN');
        $NAMA_GAMBAR    = time() . $FILE_BUKTI->getRandomName();
        $FILE_BUKTI->move(FCPATH . 'assets/foto/', $NAMA_GAMBAR);

        $DATA = [
            'STATUS'            => 'BARU',
            'BUKTI_PEMBAYARAN'  => $NAMA_GAMBAR
        ];

        $UPDATE = $this->model->updateData('TRANSACTIONS', $DATA, $GET_DATA);

        if (!$UPDATE) {
            return $this->setRespond('Gagal mengunggah bukti pembayaran!', null, 400);
        } else {
            return $this->setRespond('Berhasil mengunggah bukti pembayaran!');
        }
    }

    public function showPesananKantin()
    {
        $GET_DATA           = $this->getGet();
        $ID_CANTEEN_PROFILE = $GET_DATA['ID_CANTEEN_PROFILE'];
        $STATUS             = $GET_DATA['STATUS'];

        $PREFIX = null;
        $PESAN = 'Pesanan ';
        switch ($STATUS) {
            case 0:
                $PREFIX = "AND A.STATUS IN('BARU', 'BELUM_BAYAR') ORDER BY A.CREATED_AT ASC";
                $PESAN .= 'baru';
                break;
            case 1:
                $PREFIX = "AND A.STATUS ='DISIAPKAN' ORDER BY A.CREATED_AT ASC";
                $PESAN .= 'disiapkan';
                break;
            case 2:
                $PREFIX = "AND A.STATUS ='SELESAI' ORDER BY A.CREATED_AT DESC";
                $PESAN .= 'selesai';
                break;
            default:
                $PREFIX = "AND A.STATUS ='DITOLAK' ORDER BY A.CREATED_AT DESC";
                $PESAN .= 'ditolak';
                break;
        }

        $QUERY          = "SELECT ID_TRANSACTION, IF(ISNULL(BUKTI_PEMBAYARAN), NULL, CONCAT('" . base_url() . "assets/foto/', BUKTI_PEMBAYARAN)) AS BUKTI_PEMBAYARAN, DATE(CREATED_AT) AS CREATED_AT, TIME(CREATED_AT) AS TIME, TEMPAT_PENGANTARAN, STATUS, (SELECT SUM(X.JUMLAH * Y.HARGA) FROM DETAIL_TRANSACTIONS AS X LEFT JOIN MENUS AS Y ON(X.ID_MENU = Y.ID_MENU) WHERE X.ID_TRANSACTION = A.ID_TRANSACTION) AS TOTAL FROM TRANSACTIONS AS A WHERE ID_CANTEEN_PROFILE ='" . $ID_CANTEEN_PROFILE . "' " . $PREFIX;
        $DATA_PESANAN   = $this->model->queryArray($QUERY);

        $QUERY          = "SELECT A.ID_TRANSACTION, B.ID_PROFILE, B.NAMA_LENGKAP, CONCAT('" . base_url() . "assets/foto/', B.FOTO) AS FOTO FROM TRANSACTIONS AS A RIGHT JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.ID_CANTEEN_PROFILE ='" . $ID_CANTEEN_PROFILE . "' " . $PREFIX;
        $DATA_PEMESAN   = $this->model->queryArray($QUERY);

        $QUERY          = "SELECT B.ID_TRANSACTION, C.NAMA_MENU, C.HARGA, B.JUMLAH, (C.HARGA * B.JUMLAH) AS SUB_TOTAL FROM MENUS AS C RIGHT JOIN DETAIL_TRANSACTIONS AS B ON(C.ID_MENU = B.ID_MENU) LEFT JOIN TRANSACTIONS AS A ON(B.ID_TRANSACTION = A.ID_TRANSACTION) WHERE A.ID_CANTEEN_PROFILE='" . $ID_CANTEEN_PROFILE . "' " . $PREFIX;
        $DATA_DETAIL    = $this->model->queryArray($QUERY);

        $DATA = array();
        foreach ($DATA_PESANAN as $K => $VAL) {
            $TEMP_DETAIL = array();
            foreach ($DATA_DETAIL as $VAL1) {
                if (hash_equals($VAL['ID_TRANSACTION'], $VAL1['ID_TRANSACTION'])) {
                    unset($VAL1['ID_TRANSACTION']);
                    array_push($TEMP_DETAIL, $VAL1);
                }
            }

            unset($DATA_PEMESAN[$K]['ID_TRANSACTION']);

            $VAL['PEMESAN']         = $DATA_PEMESAN[$K];
            $VAL['DETAIL_PESANAN']  = $TEMP_DETAIL;

            array_push($DATA, $VAL);
        }

        if (count($DATA_PESANAN) < 1) {
            return $this->setRespond('Data ' . $PESAN . ' belum tersedia!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data', $DATA);
        }
    }

    public function updateStatus()
    {
        $GET_DATA   = $this->getGet();
        $RAW_DATA   = $this->getRaw();

        $ID_TRANSACTION = $GET_DATA['ID_TRANSACTION'];
        $ID_USER        = $GET_DATA['ID_USER'];
        $STATUS         = $RAW_DATA['STATUS'];

        if (hash_equals($STATUS, 'DISIAPKAN')) {
            $RAW_DATA['WAKTU_DISIAPKAN'] = $this->getTime();
        }

        $PESAN = "Pesanan anda ";
        $TOAST = 'Pesanan berhasil ';
        switch ($STATUS) {
            case 'DISIAPKAN':
                $PESAN .= 'sedang disiapkan!';
                $TOAST .= 'disiapkan!';
                break;
            case 'DITOLAK':
                $PESAN .= ' ditolak oleh penjual!';
                $TOAST .= 'ditolak!';
                break;
            default:
                $PESAN .= ' sudah siap, penjual akan mengantarkan kepada anda!';
                $TOAST .= 'diselesaikan!';
                break;
        }

        $DATA_USER = $this->model->getRowDataColumnWhereArray('USERS', 'TOKEN', ['ID_USER' => $ID_USER]);
        $this->notif->sendNotif($DATA_USER['TOKEN'], 'Pemberitahuan!', $PESAN);

        $UPDATE = $this->model->updateData('TRANSACTIONS', $RAW_DATA, ['ID_TRANSACTION' => $ID_TRANSACTION]);

        if (!$UPDATE) {
            return $this->setRespond('Gagal mengubah status pesanan!', null, 400);
        } else {
            return $this->setRespond($TOAST);
        }
    }

    public function showRekap()
    {
        $GET_DATA   = $this->getGet();

        $ID_CANTEEN_PROFILE = $GET_DATA['ID_CANTEEN_PROFILE'];
        $TANGGAL            = $GET_DATA['TANGGAL'];

        $QUERY = "SELECT COUNT(STATUS) AS COUNT FROM TRANSACTIONS WHERE ID_CANTEEN_PROFILE = '" . $ID_CANTEEN_PROFILE . "' AND DATE(CREATED_AT) = '" . $TANGGAL . "' AND STATUS = 'SELESAI'";
        $COUNT = $this->model->queryRowArray($QUERY);
        $DATA['COUNT_SELESAI'] = $COUNT['COUNT'];

        $QUERY = "SELECT COUNT(STATUS) AS COUNT FROM TRANSACTIONS WHERE ID_CANTEEN_PROFILE = '" . $ID_CANTEEN_PROFILE . "' AND DATE(CREATED_AT) = '" . $TANGGAL . "' AND STATUS = 'DITOLAK'";
        $COUNT = $this->model->queryRowArray($QUERY);
        $DATA['COUNT_DITOLAK'] = $COUNT['COUNT'];

        $QUERY = "SELECT SUM(TEMP.SUMS) AS TOTAL FROM (SELECT SUM(X.JUMLAH * Y.HARGA) AS SUMS FROM DETAIL_TRANSACTIONS AS X LEFT JOIN MENUS AS Y ON(X.ID_MENU = Y.ID_MENU) LEFT JOIN TRANSACTIONS AS W ON(X.ID_TRANSACTION = W.ID_TRANSACTION) WHERE W.ID_CANTEEN_PROFILE ='" . $ID_CANTEEN_PROFILE . "' AND DATE(W.CREATED_AT) ='" . $TANGGAL . "' AND W.STATUS ='SELESAI') AS TEMP";
        $COUNT = $this->model->queryRowArray($QUERY);
        $DATA['TOTAL'] = $COUNT['TOTAL'];

        $QUERY = "SELECT ID_TRANSACTION, STATUS, TIME(CREATED_AT) AS WAKTU, (SELECT SUM(X.JUMLAH * Y.HARGA) FROM DETAIL_TRANSACTIONS AS X LEFT JOIN MENUS AS Y ON(X.ID_MENU = Y.ID_MENU) WHERE X.ID_TRANSACTION = A.ID_TRANSACTION) AS TOTAL FROM TRANSACTIONS AS A WHERE ID_CANTEEN_PROFILE ='" . $ID_CANTEEN_PROFILE . "' AND DATE(CREATED_AT) ='" . $TANGGAL . "' AND STATUS ='SELESAI'";
        $DATA['DATA'] = $this->model->queryArray($QUERY);
        
        if (count($DATA['DATA']) < 1) {
            return $this->setRespond('Belum ada pesanan hari ini!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $DATA);
        }

    }

    public function showDetailRekap()
    {
        $GET_DATA       = $this->getGet();
        $ID_TRANSACTION = $GET_DATA['ID_TRANSACTION'];

        $QUERY = "SELECT A.TEMPAT_PENGANTARAN, DATE(A.CREATED_AT) AS TANGGAL, TIME(A.CREATED_AT) AS WAKTU, B.NAMA_LENGKAP FROM TRANSACTIONS AS A LEFT JOIN PROFILES AS B ON(A.ID_USER = B.ID_PROFILE) WHERE A.ID_TRANSACTION ='" . $ID_TRANSACTION . "'";
        $DATA_TRANSACTION = $this->model->queryRowArray($QUERY);

        $QUERY = "SELECT A.NAMA_MENU, A.HARGA, B.JUMLAH FROM MENUS AS A RIGHT JOIN DETAIL_TRANSACTIONS AS B ON(A.ID_MENU = B.ID_MENU) WHERE B.ID_TRANSACTION ='" . $ID_TRANSACTION . "'";
        $DATA_PESANAN = $this->model->queryArray($QUERY);

        $DATA_TRANSACTION['DATA_PESANAN'] = $DATA_PESANAN;

        return $this->setRespond('Berhasil mendapatkan data!', $DATA_TRANSACTION);
    }


}
