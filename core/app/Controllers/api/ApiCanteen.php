<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiCanteen extends BaseApi
{

    public function show()
    {
        $QUERY      = "SELECT B.ID_USER, A.ID_CANTEEN_PROFILE, A.NAMA_CANTEEN, CONCAT('" . base_url() . "assets/foto/', A.BANNER) AS BANNER, A.NOMOR_CANTEEN, IF((STRCMP(A.STATUS_BUKA, 'BUKA') = 0), IF(TIME(NOW()) > A.JAM_TUTUP, 'TUTUP', A.STATUS_BUKA), A.STATUS_BUKA) AS STATUS_BUKA FROM CANTEEN_PROFILES AS A LEFT JOIN USERS AS B ON(A.ID_PROFILE = B.ID_USER) WHERE B.DELETED_AT IS NULL ORDER BY CREATED_AT DESC";
        $CEK_DATA = $this->model->queryArray($QUERY);
        if (count($CEK_DATA) < 1) {
            return $this->setRespond('Data kantin kosong!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function showDetail()
    {
        $GET_DATA   = $this->getGet();

        $QUERY      = "SELECT B.ID_USER, A.NAMA_LENGKAP, A.TELEPON, B.USERNAME, B.EMAIL, C.NAMA_CANTEEN, CONCAT('" . base_url() . "assets/foto/', C.BANNER) AS BANNER, CONCAT('" . base_url() . "assets/foto/', A.FOTO) AS FOTO, C.NOMOR_CANTEEN, C.JAM_BUKA, C.JAM_TUTUP FROM PROFILES AS A INNER JOIN USERS AS B ON(A.ID_PROFILE = B.ID_USER) INNER JOIN CANTEEN_PROFILES AS C ON(C.ID_PROFILE = A.ID_PROFILE) WHERE B.ID_USER ='" . $GET_DATA['ID_USER'] . "'";
        $CEK_DATA = $this->model->queryRowArray($QUERY);
        if (is_null($CEK_DATA)) {
            return $this->setRespond('Data kantin tidak ditemukan!', null, 404);
        } else {
            return $this->setRespond('Berhasil mendapatkan data!', $CEK_DATA);
        }
    }

    public function delete()
    {
        $GET_DATA   = $this->getGet();

        $UPDATE = $this->model->updateData('USERS', ['DELETED_AT' => $this->getTimeStamp()], ['ID_USER' => $GET_DATA['ID_USER']]);
        if (!$UPDATE) {
            return $this->setRespond('Gagal menghapus kantin!', null, 400);
        } else {
            return $this->setRespond('Berhasil menghapus kantin!');
        }
    }

    public function insert()
    {
        $POST_DATA      = $this->getPost();
        $FILE_FOTO      = $this->getFile('FOTO');
        $FILE_BANNER    = $this->getFile('BANNER');

        $NAMA_FOTO      = time() . $FILE_FOTO->getRandomName();
        $NAMA_BANNER    = time() . $FILE_BANNER->getRandomName();

        $ID_USER    = 'USRS-' . strtoupper(random_string('alnum', 15));
        $ID_CANTEEN = 'CNTN-' . strtoupper(random_string('alnum', 15));

        $DATA = [
            'ID_USER'   => $ID_USER,
            'USERNAME'  => $POST_DATA['USERNAME'],
            'EMAIL'     => $POST_DATA['EMAIL'],
            'PASSWORD'  => password_hash($POST_DATA['PASSWORD'], PASSWORD_BCRYPT),
            'LEVEL'     => 'PENJUAL',
        ];
        $this->model->insertData('USERS', $DATA);



        $FILE_FOTO->move(FCPATH . 'assets/foto/', $NAMA_FOTO);
        $DATA = [
            'ID_PROFILE'    => $ID_USER,
            'NAMA_LENGKAP'  => ucfirst($POST_DATA['NAMA_LENGKAP']),
            'TELEPON'       => $POST_DATA['TELEPON'],
            'FOTO'          => $NAMA_FOTO
        ];
        $this->model->insertData('PROFILES', $DATA);



        $FILE_BANNER->move(FCPATH . 'assets/foto/', $NAMA_BANNER);
        $DATA = [
            'ID_CANTEEN_PROFILE'    => $ID_CANTEEN,
            'NAMA_CANTEEN'          => $POST_DATA['NAMA_CANTEEN'],
            'BANNER'                => $NAMA_BANNER,
            'JAM_BUKA'              => $POST_DATA['JAM_BUKA'],
            'JAM_TUTUP'             => $POST_DATA['JAM_TUTUP'],
            'NOMOR_CANTEEN'         => $POST_DATA['NOMOR_CANTEEN'],
            'ID_PROFILE'            => $ID_USER
        ];
        $this->model->insertData('CANTEEN_PROFILES', $DATA);

        return $this->setRespond('Berhasil menambahkan kantin!');
    }

    public function update()
    {
        $POST_DATA      = $this->getPost();
        $GET_DATA       = $this->getGet();
        $FILE_FOTO      = $this->getFile('FOTO');
        $FILE_BANNER    = $this->getFile('BANNER');

        $ID_USER        = $GET_DATA['ID_USER'];

        // return $this->setRespond($POST_DATA);

        $DATA = [
            'EMAIL'     => $POST_DATA['EMAIL'],
        ];
        if (isset($POST_DATA['PASSWORD'])) {
            $DATA['PASSWORD'] = password_hash($POST_DATA['PASSWORD'], PASSWORD_BCRYPT);
        }
        $this->model->updateData('USERS', $DATA, $GET_DATA);



        $DATA = [
            'NAMA_LENGKAP'  => ucfirst($POST_DATA['NAMA_LENGKAP']),
            'TELEPON'       => $POST_DATA['TELEPON'],
        ];
        if ($FILE_FOTO != null) {
            $NAMA_FOTO      = time() . $FILE_FOTO->getRandomName();
            $DATA['FOTO'] = $NAMA_FOTO;
            $FILE_FOTO->move(FCPATH . 'assets/foto/', $NAMA_FOTO);
        }
        $this->model->updateData('PROFILES', $DATA, ['ID_PROFILE' => $ID_USER]);



        $DATA = [
            'NAMA_CANTEEN'          => $POST_DATA['NAMA_CANTEEN'],
            'JAM_BUKA'              => $POST_DATA['JAM_BUKA'],
            'JAM_TUTUP'             => $POST_DATA['JAM_TUTUP'],
        ];
        if ($FILE_BANNER != null) {
            $NAMA_BANNER    = time() . $FILE_BANNER->getRandomName();
            $DATA['BANNER'] = $NAMA_BANNER;
            $FILE_BANNER->move(FCPATH . 'assets/foto/', $NAMA_BANNER);
        }
        $this->model->updateData('CANTEEN_PROFILES', $DATA, ['ID_PROFILE' => $ID_USER]);


        return $this->setRespond('Berhasil mengubah kantin!');
    }
}
