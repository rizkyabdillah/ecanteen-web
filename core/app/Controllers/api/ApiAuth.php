<?php

namespace App\Controllers\api;

use App\Controllers\BaseApi;

class ApiAuth extends BaseApi
{

    public function login()
    {
        $RAW_DATA  = $this->getRaw();

        $QUERY      = "SELECT ID_USER, PASSWORD, LEVEL FROM USERS WHERE (USERNAME ='" . $RAW_DATA['USEREMAIL'] . "' OR EMAIL ='" . $RAW_DATA['USEREMAIL'] . "') AND DELETED_AT IS NULL";
        $CEK_DATA = $this->model->queryRowArray($QUERY);

        if (is_null($CEK_DATA)) {
            return $this->setRespond('Gagal login', null, 400, [$this->setError('USEREMAIL', 'Username atau email anda tidak terdaftar!')]);
        } else {
            $CEK_PASS = password_verify($RAW_DATA['PASSWORD'], $CEK_DATA['PASSWORD']);
            if ($CEK_PASS) {
                $QUERY          = "SELECT A.ID_USER, A.EMAIL, A.LEVEL, B.NAMA_LENGKAP, B.TELEPON, IF(ISNULL(B.FOTO), NULL, CONCAT('" . base_url() . "assets/foto/', B.FOTO)) AS FOTO FROM USERS AS A INNER JOIN PROFILES AS B ON (A.ID_USER = B.ID_PROFILE) WHERE A.ID_USER ='" . $CEK_DATA['ID_USER'] . "'";
                $DATA_PROFILE   = $this->model->queryRowArray($QUERY);

                $DATA = [
                    'DATA_PROFILE' => $DATA_PROFILE
                ];

                if (hash_equals($CEK_DATA['LEVEL'], 'PENJUAL')) {
                    $QUERY                  = "SELECT ID_CANTEEN_PROFILE, NAMA_CANTEEN, CONCAT('" . base_url() . "assets/foto/', BANNER) AS BANNER FROM CANTEEN_PROFILES WHERE ID_PROFILE ='" . $CEK_DATA['ID_USER'] . "'";
                    $DATA_CANTEEN           = $this->model->queryRowArray($QUERY);
                    $DATA['DATA_CANTEEN']   = $DATA_CANTEEN;
                }

                $this->model->updateData('USERS', ['TOKEN' => $RAW_DATA['TOKEN']], ['ID_USER' => $CEK_DATA['ID_USER']]);

                return $this->setRespond('Selamat datang!', $DATA);
            } else {
                return $this->setRespond('Gagal login', null, 400, [$this->setError('PASSWORD', 'Password anda salah!')]);
            }
        }
    }

    public function register()
    {
        $RAW_DATA  = $this->getRaw();

        $CEK_USERNAME   = $this->model->getRowDataArray('USERS',    ['USERNAME'     =>  $RAW_DATA['USERNAME']]);
        $CEK_EMAIL      = $this->model->getRowDataArray('USERS',    ['EMAIL'        =>  $RAW_DATA['EMAIL']]);
        $CEK_TELEPON    = $this->model->getRowDataArray('PROFILES', ['TELEPON'      =>  $RAW_DATA['TELEPON']]);

        $ERROR = [];
        $isValid = true;

        if (!is_null($CEK_USERNAME)) {
            array_push($ERROR, $this->setError('USERNAME', 'Username tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_EMAIL)) {
            array_push($ERROR, $this->setError('EMAIL', 'Email tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!is_null($CEK_TELEPON)) {
            array_push($ERROR, $this->setError('TELEPON', 'Nomor telepon tersebut sudah terdaftar'));
            $isValid = false;
        }

        if (!$isValid) {
            return $this->setRespond('Gagal registrasi!', null, 400, $ERROR);
        }

        $ID_USER    = 'USRS-' . strtoupper(random_string('alnum', 15));
        $DATA = [
            'ID_USER'       => $ID_USER,
            'USERNAME'      => $RAW_DATA['USERNAME'],
            'EMAIL'         => $RAW_DATA['EMAIL'],
            'PASSWORD'      => password_hash($RAW_DATA['PASSWORD'], PASSWORD_BCRYPT),
            'LEVEL'         => $RAW_DATA['LEVEL'],
            'TOKEN'         => $RAW_DATA['TOKEN'],
        ];

        $this->model->insertData('USERS', $DATA);


        $DATA = [
            'ID_PROFILE'    => $ID_USER,
            'TELEPON'       => $RAW_DATA['TELEPON'],
            'NAMA_LENGKAP'  => $RAW_DATA['NAMA_LENGKAP']
        ];

        $this->model->insertData('PROFILES', $DATA);

        $QUERY          = "SELECT A.ID_USER, A.EMAIL, A.LEVEL, B.NAMA_LENGKAP, B.TELEPON, IF(ISNULL(B.FOTO), NULL, CONCAT('" . base_url() . "assets/foto/', B.FOTO)) AS FOTO FROM USERS AS A INNER JOIN PROFILES AS B ON (A.ID_USER = B.ID_PROFILE) WHERE A.ID_USER ='" . $ID_USER . "'";
        $DATA_PROFILE   = $this->model->queryRowArray($QUERY);

        $DATA = [
            'DATA_PROFILE' => $DATA_PROFILE
        ];

        return $this->setRespond('Registrasi berhasil, selamat datang!', $DATA);
    }
}
