<?php

namespace App\Controllers;

class BaseApi extends BaseController
{

    private $codes = [
        200     => 'OK',
        201     => 'HTTP CREATED',
        400     => 'BAD REQUEST',
        403     => 'FORBIDDEN',
        404     => 'NOT FOUND',
        500     => 'INTERNAL SERVER ERROR',
        502     => 'BAD GATEWAY',
        504     => 'GATEWAY TIMEOUT',
    ];


    protected function setRespond($message, $data = null, $code = 200, $error = null)
    {
        $ARRAY = [
            'code'      => $code,
            'status'    => $this->codes[$code],
            'message'   => $message,
        ];

        if (!is_null($error)) {
            $ARRAY['error']  = $error;
        }

        if (!is_null($data)) {
            $ARRAY['data']  = $data;
        }

        return $this->response->setJSON($ARRAY)->setStatusCode($code);
    }

    protected function setError($name, $message)
    {
        return [
            'NAME'      => $name,
            'MESSAGE'   => $message
        ];
    }

    protected function getTimeStamp() {
        return date('Y-m-d H:i:s');
    }

    protected function getTime() {
        return date('H:i');
    }
}
